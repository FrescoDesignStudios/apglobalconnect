package com.mysticlightsstudios.www.apglobalconnect;

import android.Manifest;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.AnimationDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Nullable;


public class DashboardActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    public EditText trackerPhoneNumber;
    private String activePhoneNumber;
    String response;
    String deviceNumber;
    private int password, indexCount;
    public String available, passwordString; // for check the power state of the car
    public int counter;
    private String documentID;
    private String status;
    public String lat;
    public String longi;
    public Timer timer;

    private int defaultTime = 1;


    public void listen(View view) {
        // check if the device is set to monitor or track and has not been set as yet

        String UID = mAuth.getCurrentUser().getUid();


        db.collection("users").document(documentID).collection("status").document(documentID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {

                            DocumentSnapshot document = task.getResult();

                            String monitor;

                            try {
                                monitor = document.getData().get("monitor").toString();
                            } catch (NullPointerException e) {
                                monitor = "off";
                            }

                            if (monitor.equals("off")) {

                                Log.d("tag", "onComplete: a mess");

                                sendSMS(deviceNumber, "monitor" + password);

                                if (ContextCompat.checkSelfPermission(DashboardActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(DashboardActivity.this, new String[]{Manifest.permission.CALL_PHONE},1);
                                }
                                else
                                {
                                    Timer timer = new Timer();
                                    timer.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                                            callIntent.setData(Uri.parse("tel:" + deviceNumber));
                                            startActivity(callIntent);
                                        }
                                    }, 6000);
                                }

                                // set monitor to on
                                HashMap<String, Object> map = new HashMap<>();
                                map.put("monitor","on");

                                db.collection("users").document(documentID).collection("status").document(documentID)
                                        .update(map);


                            }else{

                            }

                        }else{

                        }
                    }
                });
    }

    public void passwordChange(View view){
        Intent password = new Intent(DashboardActivity.this, PasswordActivity.class);
        startActivity(password);
    }

    public void settings(View view){
        LinearLayout settingLayout = findViewById(R.id.settings);


        openAnimation(settingLayout);

    }

    public void closeSettings(View view){
        LinearLayout settingLayout = findViewById(R.id.settings);

        closeAnimation(settingLayout);
    }

    public void billing(View view){
        Intent billing = new Intent(DashboardActivity.this, BillingActivity.class);
        DashboardActivity.this.startActivity(billing);
    }

    public void logout(View view){
        FirebaseAuth.getInstance().signOut();
        Intent login = new Intent(DashboardActivity.this, MainActivity.class);
        DashboardActivity.this.startActivity(login);
    }

    public void arm(View view){
        String UID = mAuth.getCurrentUser().getUid();

        db.collection("users").document(documentID).collection("status").document(documentID)
        .get()
        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()){

                    DocumentSnapshot document = task.getResult();
                    ImageView arm = findViewById(R.id.arm);

                    if (document.exists()){

                        String armAlarm;

                        try {
                           armAlarm = document.getData().get("arm").toString();
                        }catch (NullPointerException e){
                            armAlarm = "null";
                        }
                        // check if the alarm is on

                        if (armAlarm.equals("on")){
                            arm.setImageResource(R.drawable.arm);

                            sendSMS(deviceNumber, "disarm"+password);

                            HashMap<String, Object> stopUpdate = new HashMap<>();
                            stopUpdate.put("device", deviceNumber);
                            stopUpdate.put("arm", "off");

                            db.collection("users").document(documentID).collection("status").document(documentID)
                                    .update(stopUpdate);

                            HashMap<String, Object> map2 = new HashMap<>();
                            map2.put("securityAlarm", "off");

                            db.collection("users").document(documentID).collection("status").document(documentID)
                                    .update(map2);

                        }else{

                            arm.setImageResource(R.drawable.arm2);

                            try {
                                sendSMS(deviceNumber, "arm"+password);
                            }catch (IllegalArgumentException e){
                                Toast.makeText(getApplicationContext(), "Error:M001 - "+e.getMessage(), Toast.LENGTH_LONG).show();
                            }


                            HashMap<String, Object> stopUpdate = new HashMap<>();
                            stopUpdate.put("device", deviceNumber);
                            stopUpdate.put("arm", "on");

                            String UID = mAuth.getCurrentUser().getUid();
                            db.collection("users").document(documentID).collection("status").document(documentID)
                                    .update(stopUpdate);

                        }


                    }else{
                        HashMap<String, Object> stopUpdate = new HashMap<>();
                        stopUpdate.put("device", deviceNumber);
                        stopUpdate.put("arm", "off");

                        String UID = mAuth.getCurrentUser().getUid();
                        db.collection("users").document(documentID).collection("status").document(documentID)
                                .set(stopUpdate);
                    }

                    ConstraintLayout layout = findViewById(R.id.overlay);
                    layout.setVisibility(View.VISIBLE);
                    

                }else{

                }
            }
        });


    }

    public void shutdownCar(View view){

        // check the status of the shutoff
        String UID = mAuth.getCurrentUser().getUid();
        db.collection("users").document(documentID).collection("status").document(documentID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if(task.isSuccessful()){

                            DocumentSnapshot document = task.getResult();
                            ImageView powerButton = findViewById(R.id.powerButton);

                            if (document.exists()){

                                try{
                                     status = document.getData().get("power").toString();
                                }catch (NullPointerException e){
                                     status = "off";
                                }

                                    if(status.equals("on")){

                                        powerButton.setImageResource(R.drawable.power2);
                                        sendSMS(deviceNumber, "stop"+password);

                                        HashMap<String, Object> stopUpdate = new HashMap<>();
                                        stopUpdate.put("device", deviceNumber);
                                        stopUpdate.put("power", "off"); // car is not allowed to be on

                                        String UID = mAuth.getCurrentUser().getUid();
                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                .update(stopUpdate);

                                    }else{

                                        powerButton.setImageResource(R.drawable.power);
                                        sendSMS(deviceNumber, "resume"+password);

                                        HashMap<String, Object> stopUpdate = new HashMap<>();
                                        stopUpdate.put("device", deviceNumber);
                                        stopUpdate.put("power", "on"); // car is allowed to be on

                                        String UID = mAuth.getCurrentUser().getUid();
                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                .update(stopUpdate);
                                    }




                            }else{
                                sendSMS(deviceNumber, "resume"+password);
                                powerButton.setImageResource(R.drawable.power2);

                                HashMap<String, Object> stopUpdate = new HashMap<>();
                                stopUpdate.put("device", deviceNumber);
                                stopUpdate.put("power", "off");

                                String UID = mAuth.getCurrentUser().getUid();
                                db.collection("users").document(documentID).collection("status").document(documentID)
                                        .set(stopUpdate);

                            }


                            ConstraintLayout layout = findViewById(R.id.overlay);
                            layout.setVisibility(View.VISIBLE);




                        }else{
                            Log.d("DashboardActivity", "Error:", task.getException());
                        }
                    }
                });

        ConstraintLayout layout = findViewById(R.id.overlay);
        layout.setVisibility(View.VISIBLE);

        HashMap<String, Object> map = new HashMap<>();
        map.put("received","false");
        db.collection("users").document(documentID).collection("dump").document(documentID)
                .update(map);

    }

    public void addNewDeviceButton(View view){


        Spinner carMakeEdit = findViewById(R.id.carMakeEdit);
        EditText makeModelEdit = findViewById(R.id.makeModelEdit);
        EditText  carYear = findViewById(R.id.carYear);
        EditText userPhoneNumber = findViewById(R.id.userPhoneNumber);
        LinearLayout makeModel = findViewById(R.id.makeModel);

        // Strings to be inserted
        String brand = carMakeEdit.getSelectedItem().toString();
        String model = makeModelEdit.getText().toString();
        String year = carYear.getText().toString();
        String userPhone = userPhoneNumber.getText().toString();

        Random rand = new Random();
        int n = rand.nextInt(300000) + 99999;

        HashMap<String, Object> carInfo = new HashMap<>();
        carInfo.put("brand", brand);
        carInfo.put("model", model);
        carInfo.put("year", year);
        carInfo.put("password", n);
        carInfo.put("userPhone", userPhone);

        String UID = mAuth.getCurrentUser().getUid();
        db.collection("users").document(documentID).collection("devices").document(activePhoneNumber)
        .update(carInfo);


        // send new password to number
        sendSMS(activePhoneNumber, "password123456 "+n);
       // sendSMS(activePhoneNumber, "Admin+"+n+userPhone);

        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", -410f);
        ObjectAnimator animation2 = ObjectAnimator.ofFloat(view, "alpha", 0f);
        animation.setDuration(500);
        animation.setTarget(makeModel);

        animation2.setDuration(200);
        animation2.setTarget(findViewById(R.id.settingsAddNewDevice));

        AnimatorSet animationSet = new AnimatorSet();
        animationSet.playTogether(animation,animation2);

        animationSet.start();

        Intent intent = getIntent();
        finish();
        startActivity(intent);

    }

    public void addTrackingDevice(View view){
        // LinearLayout settingLayout = findViewById(R.id.settingsAddNewDevice);
        LinearLayout setting = findViewById(R.id.settings);
        EditText addTrackerNumber = findViewById(R.id.addTrackerNumber);

        Intent tracking = new Intent(DashboardActivity.this, ManageDevices.class);
        DashboardActivity.this.startActivity(tracking);

    }

    public void addTrackingDeviceNumber(View view){
        trackerPhoneNumber = findViewById(R.id.addTrackerNumber);
        Spinner makeDropDown  = findViewById(R.id.carMakeEdit);
        LinearLayout finalLay = findViewById(R.id.makeModel);

        String phoneNumber = trackerPhoneNumber.getText().toString();
        activePhoneNumber = phoneNumber;

        trackerPhoneNumber.setFocusable(true);


        if(phoneNumber.length() == 10){

             sendSMS(phoneNumber, "begin123456");

             String UID = mAuth.getCurrentUser().getUid();

             Map<String, Object> number = new HashMap<>();
             number.put("device", phoneNumber);
             number.put("user", UID);


             db.collection("users").document(documentID).collection("devices").document(phoneNumber)
                     .set(number);


             finalLay.bringToFront();

             String[] items = new String[]{"Acura","Audi","BMW","Chevrolet" ,"Fiat","Ford","GMC",
             "Honda","Hyundai","Infiniti","Jaguar","Jeep","Kia","Land Rover","Lexus","Maserati",
             "Mazda","McLaren","Mercedes-Benz","Mini","Mitsubishi","Pagani","Peugeot","Porsche",
             "Nissan","Ram","Subaru","Suzuki","Toyota","Volkswagen","Volvo"};

            ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.spinner_item, items);
            makeDropDown.setAdapter(adapter);


             ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 1f);
             animation.setDuration(200);
             animation.setTarget(finalLay);
             animation.start();

        }else{
            Toast.makeText(getApplicationContext(), "Phone Number too Short", Toast.LENGTH_LONG).show();
        }
    }

    public void userCollectionUpdate(final String value, final String key, final LinearLayout first, final LinearLayout second){

        String UID = mAuth.getCurrentUser().getUid();
        Map<String, Object> user = new HashMap<>();

        user.put(key, value);

        db.collection("users").document(documentID)
                .update(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        //RelativeLayout lname = findViewById(R.id.lastName);
                        //RelativeLayout tracking = findViewById(R.id.tracking);

                        first.animate().alpha(0f).setDuration(1000);

                        second.animate().alpha(1f).setDuration(1000).setStartDelay(500);


                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        userCollectionSet( value, key, first, second);
                    }
                });
    }

    public void userCollectionSet(String value, String key, final LinearLayout first, final LinearLayout second){
        String UID = mAuth.getCurrentUser().getUid();
        Map<String, Object> user = new HashMap<>();

        user.put(key, value);

        db.collection("users").document(documentID)
                .set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {

                        //RelativeLayout lname = findViewById(R.id.lastName);
                        //RelativeLayout tracking = findViewById(R.id.tracking);

                        first.animate().alpha(0f).setDuration(1000);

                        second.animate().alpha(1f).setDuration(1000).setStartDelay(500);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        String errorMessage = exception.getMessage();
                        Toast.makeText(getApplicationContext(), errorMessage, Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void openAnimation(LinearLayout layout){
        @SuppressLint("ObjectAnimatorBinding") ObjectAnimator animation = ObjectAnimator.ofFloat(this, "translationX", 1f);
        animation.setDuration(200);
        animation.setTarget(layout);
        animation.start();
    }

    public void closeAnimation(LinearLayout layout){
        @SuppressLint("ObjectAnimatorBinding") ObjectAnimator animation = ObjectAnimator.ofFloat(this, "translationX", -777f);
        animation.setDuration(200);
        animation.setTarget(layout);
        animation.start();
    }

     // nav effects

    public void tracking(View view){
        Intent tracking = new Intent(DashboardActivity.this, TrackingActivity.class);
        DashboardActivity.this.startActivity(tracking);
    }

    public void checkCredit(final String document){



        db.collection("users").document(document).collection("billing").document(document)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){
                            Log.d("checkCredit: ", " "+document);
                            DocumentSnapshot snapshot = task.getResult();

                            if (snapshot.exists()){
                                String credit = snapshot.get("credit").toString();

                                if (credit.equals("0")){
                                    Intent reload = new Intent(DashboardActivity.this, ReloadActivity.class);
                                    startActivity(reload);
                                }

                            }else{

                            }

                        }else{

                        }
                    }
                });
    }

    public void changeInt(View view){
        db.collection("users").document(documentID).collection("status").document(documentID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){

                            DocumentSnapshot documentSnapshot = task.getResult();
                            String refresh = documentSnapshot.getData().get("refresh").toString();
                            //HashMap<String, Object> map = new HashMap<>();
                            TextView textView = findViewById(R.id.rate);

                            long startTime;

                            switch (refresh){
                                case "1hr":

                                    db.collection("users").document(documentID).collection("status").document(documentID)
                                            .update("refresh", "2hr");

                                    startTime = 14400 *1000;
                                    timer.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                        }
                                    },startTime, startTime);



                                    textView.setText("2hr");
                                    break;

                                case "2hr":

                                    db.collection("users").document(documentID).collection("status").document(documentID)
                                            .update("refresh", "INS");


                                    sendSMS(deviceNumber, "fix030s001n"+passwordString);

                                    textView.setText("INS");

                                    break;

                                case "INS":

                                    db.collection("users").document(documentID).collection("status").document(documentID)
                                            .update("refresh", "1hr");

                                    startTime = 3600 * 1000;

                                    timer.schedule(new TimerTask() {
                                        @Override
                                        public void run() {
                                            sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                        }
                                    },startTime, startTime);


                                    textView.setText("1hr");
                                    break;

                                default:

                            }

                        }else{

                        }
                    }
                });
    }


    public void sendSMS(String phoneNumber, String message){



        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";


        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:

                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));


        if ( ContextCompat.checkSelfPermission( this, Manifest.permission.SEND_SMS ) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions( this, new String[] {  Manifest.permission.SEND_SMS  },
                    3 );
        }else{

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 4);

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 5);
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
                }else{
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
                }

            }else{
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
            }

        }


    }

    public void alerts(View view){
        Intent alert = new Intent(DashboardActivity.this, AlertActivity.class);
        startActivity(alert);
    }

    public void terms(View view){
        Intent alert = new Intent(DashboardActivity.this, TermsActivity.class);
        startActivity(alert);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 4) {
            // YES!!
            Log.i("TAG", "MY_PERMISSIONS_REQUEST_SMS_RECEIVE --> YES");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_dashboard);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        //FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
          //      .setTimestampsInSnapshotsEnabled(true)
          //      .build();
       // db.setFirestoreSettings(settings);

        timer = new Timer();


        if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS},5);
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.CALL_PHONE}, 6);

        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, 2);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},3);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_MMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS}, 4);
        }



        // check if user is signed in
        LinearLayout settingsLayout = findViewById(R.id.settings);
        settingsLayout.bringToFront();

        if (mAuth.getCurrentUser() == null){
            Intent check = new Intent(DashboardActivity.this, MainActivity.class);
            DashboardActivity.this.startActivity(check);
        }else{

            // check users account info
            final String UID = mAuth.getCurrentUser().getUid();
            db.collection("users")
                    .whereEqualTo("UID", UID)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()){

                                for (QueryDocumentSnapshot snapshot :task.getResult()){
                                    //get first device details

                                    String stUID;

                                    try{
                                        stUID = snapshot.getData().get("UID").toString();
                                    }catch (NullPointerException e){
                                        stUID = "null";
                                    }

                                    if (stUID.equals(UID)){
                                        documentID = snapshot.getId();

                                        checkCredit(documentID);

                                        // start listening for when a change in made in the database

                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                                                        if (e != null) {
                                                            Log.w("listenFailure", "Listen failed", e);
                                                        }

                                                        if (documentSnapshot != null && documentSnapshot.exists()){
                                                            // run code to make the icon blink

                                                            try {
                                                                String state = documentSnapshot.get("securityAlarm").toString();

                                                                AnimationDrawable animationDrawable = new AnimationDrawable();
                                                                animationDrawable.addFrame(getDrawable(R.drawable.car_outline),800);
                                                                animationDrawable.addFrame(getDrawable(R.drawable.car_outline2), 800);
                                                                animationDrawable.setOneShot(false);

                                                                ImageView imageAnimation = findViewById(R.id.carIconMain);
                                                                TextView textView = findViewById(R.id.secureInfo);
                                                                imageAnimation.setImageDrawable(animationDrawable);

                                                                MediaPlayer mp;
                                                                mp = MediaPlayer.create(getApplicationContext(), R.raw.alarm);

                                                                if (state.equals("on")){

                                                                    animationDrawable.start();
                                                                    textView.setText("Attention");
                                                                    mp.start();
                                                                }

                                                                if (state.equals("off")){

                                                                    animationDrawable.stop();
                                                                    textView.setText("Secured");
                                                                    mp.stop();
                                                                }



                                                            }catch(NullPointerException vul) {
                                                                Log.e("SecuirtyAlarm", "Secuirty alarm is empty");
                                                            }


                                                            try {
                                                                String arm = documentSnapshot.get("arm").toString();

                                                                if (arm.contains("off")){
                                                                    ImageView armImage = findViewById(R.id.arm);
                                                                    armImage.setImageResource(R.drawable.arm);



                                                                }

                                                            }catch(NullPointerException e1){

                                                            }
                                                        }
                                                    }
                                                });


                                    db.collection("users").document(documentID).collection("devices")
                                            .limit(1)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if(task.isSuccessful()){

                                                        for(final QueryDocumentSnapshot document : task.getResult()){


                                                            String number = document.getData().get("device").toString();

                                                            Log.i("device", number);


                                                            counter++;
                                                            TextView carName = findViewById(R.id.carName);
                                                            if(document.getData().get("model") == null || document.getData().get("brand") == null){
                                                                carName.setText("No Device");
                                                                deviceNumber = "No Device";
                                                            }else{


                                                                deviceNumber = document.getData().get("device").toString();

                                                                try {
                                                                    password = Integer.parseInt(document.getData().get("password").toString());
                                                                }catch (NullPointerException e){
                                                                    password = 0;
                                                                }


                                                                if(password == 0){
                                                                    //create a password for device
                                                                    //int min = 100000;
                                                                    //int max = 999999;
                                                                    //int random = new Random().nextInt(max - min) + min;

                                                                    password = 123456;

                                                                    HashMap<String, Object> map = new HashMap<>();
                                                                    map.put("password", "123456");

                                                                    db.collection("users").document(documentID).collection("devices").document(number)
                                                                            .update(map);

                                                                    sendSMS(deviceNumber, "begin123456");
                                                                    //sendSMS(deviceNumber, "password123456 "+String.valueOf(password));



                                                                }
                                                                String yearString =  document.getData().get("year").toString();

                                                                carName.setText(document.getData().get("brand") +" "+ document.getData().get("model") +" "+yearString);

                                                                // check the response that was stored in the database

                                                                try {
                                                                    response = document.getData().get("response").toString();
                                                                }catch (NullPointerException e)
                                                                {
                                                                    response = "null";
                                                                }

                                                                deviceNumber = document.getData().get("device").toString();


                                                                if(response.equals("begin ok!") || response.equals("password ok!") || response.equals("admin ok!")){

                                                                    passwordString = document.getData().get("password").toString();

                                                                    // set the check tracker to a certian amount of time


                                                                }else{

                                                                    if(response.equals("password ok!")){


                                                                    }else{


                                                                        break;

                                                                    }
                                                                }

                                                                // get the latest lat from the database
                                                                db.collection("users").document(documentID).collection("tracking")
                                                                        .orderBy("timestamp", Query.Direction.DESCENDING).limit(80)
                                                                        .get()
                                                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                                if (task.isSuccessful()){

                                                                                    indexCount = 0;

                                                                                    for (QueryDocumentSnapshot documentSnapshot : task.getResult()){



                                                                                        final String documentIdentifier = documentSnapshot.getId();

                                                                                        Date time = (Date) documentSnapshot.get("timestamp");
                                                                                        Long timestamp = time.getTime();

                                                                                        Long nowTime = new Date().getTime()-3*24*60*60*1000;

                                                                                        // timestamp 1200 nowTime 1102

                                                                                        if (timestamp > nowTime){
                                                                                            Log.i("timeSkip", timestamp+" no "+nowTime+" "+ documentIdentifier);

                                                                                        }else{

                                                                                            db.collection("users").document(documentID).collection("tracking")
                                                                                                    .document(documentIdentifier).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                                                @Override
                                                                                                public void onSuccess(Void aVoid) {
                                                                                                    Log.d("taf", "DocumentSnapshot successfully deleted!");
                                                                                                    Log.i("timeSkip", documentID);
                                                                                                }
                                                                                            })
                                                                                                    .addOnFailureListener(new OnFailureListener() {
                                                                                                        @Override
                                                                                                        public void onFailure(@NonNull Exception e) {
                                                                                                            Log.w("tag", "Error deleting document", e);
                                                                                                        }
                                                                                                    });


                                                                                        }

                                                                                            if (indexCount < 1){
                                                                                                try{

                                                                                                    // check current data
                                                                                                    lat = documentSnapshot.getData().get("lat").toString();
                                                                                                    longi = documentSnapshot.getData().get("long").toString();


                                                                                                    if(!lat.equals("null")){
                                                                                                        String latitudeRaw = lat;
                                                                                                        String longitudeRaw = longi;

                                                                                                        Double latitude = Double.parseDouble(latitudeRaw);
                                                                                                        Double longitude = Double.parseDouble(longitudeRaw);


                                                                                                        Geocoder geocoder;
                                                                                                        List<Address> addresses = null;
                                                                                                        geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                                                                                                        try {
                                                                                                            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                                                                                        } catch (IOException e1) {
                                                                                                            e1.printStackTrace();

                                                                                                        }

                                                                                                        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                                                                                        String city = addresses.get(0).getLocality();
                                                                                                        String state = addresses.get(0).getAdminArea();
                                                                                                        String country = addresses.get(0).getCountryName();
                                                                                                        String postalCode = addresses.get(0).getPostalCode();
                                                                                                        String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                                                                                                        if (state == null){
                                                                                                            state = " ";
                                                                                                        }

                                                                                                        TextView nowAt = findViewById(R.id.nowatAddress);
                                                                                                        nowAt.setText(knownName+" "+city+" "+state);
                                                                                                        Log.i("result", " "+knownName+" "+documentSnapshot.getData().get("lat"));


                                                                                                    }



                                                                                                }catch(NullPointerException e){
                                                                                                    Log.i("locationError", e.getMessage());
                                                                                                }
                                                                                            }

                                                                                        indexCount++;


                                                                                    }

                                                                                }else{

                                                                                }
                                                                            }
                                                                        });



                                                                db.collection("users").document(documentID).collection("status").document(documentID)
                                                                        .get()
                                                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                                                if (task.isSuccessful()){


                                                                                    DocumentSnapshot document = task.getResult();
                                                                                    String power;
                                                                                    try{
                                                                                        power = (String) document.getData().get("power");
                                                                                        String monitor = (String) document.getData().get("monitor");


                                                                                        ImageView powerButton = findViewById(R.id.powerButton);
                                                                                        if (power.equals("on")){
                                                                                            powerButton.setImageResource(R.drawable.power);
                                                                                            available = "yes";
                                                                                        }

                                                                                        if (power.equals("off")){
                                                                                            powerButton.setImageResource(R.drawable.power2);
                                                                                            available = "yes";
                                                                                        }

                                                                                        if (monitor.equals("on")){
                                                                                            sendSMS(deviceNumber, "tracker"+password);

                                                                                            HashMap<String, Object> map = new HashMap<>();
                                                                                            map.put("monitor", "off");

                                                                                            db.collection("users").document(documentID).collection("status").document(documentID)
                                                                                                    .update(map);
                                                                                        }

                                                                                    }catch (NullPointerException e) {
                                                                                        power = "null";
                                                                                    }


                                                                                    ImageView armImage = findViewById(R.id.arm);


                                                                                    String arm;
                                                                                    try {
                                                                                        arm = (String) document.getData().get("arm");

                                                                                        if (arm.equals("on")){
                                                                                            armImage.setImageResource(R.drawable.arm2);
                                                                                            Log.d("DashboardActivity", "arm");
                                                                                        }else{
                                                                                            armImage.setImageResource(R.drawable.arm);
                                                                                        }

                                                                                    }catch (NullPointerException e){
                                                                                        arm = "null";
                                                                                        e.getStackTrace();
                                                                                    }

                                                                                    try {
                                                                                        String refreshRate = document.getData().get("refresh").toString();


                                                                                        long startTime;
                                                                                        Handler mHandler = new Handler();



                                                                                        switch (refreshRate){
                                                                                            case "1hr":

                                                                                                startTime = 3600 * 1000;

                                                                                                timer.schedule(new TimerTask() {
                                                                                                    @Override
                                                                                                    public void run() {
                                                                                                        sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                                                                                    }
                                                                                                },startTime, startTime);
                                                                                                //Log.i("time", Long.toString(startTime) );

                                                                                                break;

                                                                                            case "2hr":

                                                                                                startTime = 14400 *1000;
                                                                                                timer.schedule(new TimerTask() {
                                                                                                    @Override
                                                                                                    public void run() {
                                                                                                        sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                                                                                    }
                                                                                                },startTime, startTime);

                                                                                                break;

                                                                                            case "INS":

                                                                                                //sendSMS(deviceNumber, "fix030s001n"+passwordString);


                                                                                            case "minute":

                                                                                                startTime = 60 *1000;
                                                                                                mHandler.postDelayed(new Runnable() {
                                                                                                    @Override
                                                                                                    public void run() {
                                                                                                      //  sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                                                                                    }
                                                                                                }, startTime);


                                                                                            default:
                                                                                               // sendSMS(deviceNumber, "fix030s001n"+passwordString);

                                                                                        }

                                                                                        TextView textView = findViewById(R.id.rate);
                                                                                        textView.setText(refreshRate);

                                                                                    }catch (NullPointerException e){
                                                                                        HashMap<String, Object> map = new HashMap<>();
                                                                                        map.put("refresh", "1hr");

                                                                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                                                                .update(map);
                                                                                    }

                                                                                }
                                                                            }
                                                                        });

                                                                db.collection("users").document(documentID).collection("dump").document(documentID)
                                                                        .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                                                            @Override
                                                                            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {


                                                                                try {
                                                                                    String document = documentSnapshot.getData().get("received").toString();
                                                                                    Log.i("receiveChanged", document);

                                                                                    if (document.equals("true")){
                                                                                        HashMap<String, Object> map = new HashMap<>();
                                                                                        map.put("received","false");
                                                                                        db.collection("users").document(documentID).collection("dump").document(documentID)
                                                                                                .update(map);

                                                                                        ConstraintLayout layout = findViewById(R.id.overlay);
                                                                                        layout.setVisibility(View.GONE);

                                                                                    }

                                                                                }catch (NullPointerException es){
                                                                                    HashMap<String, Object> map = new HashMap<>();
                                                                                    map.put("received","false");
                                                                                    db.collection("users").document(documentID).collection("dump").document(documentID)
                                                                                            .update(map);
                                                                                }
                                                                            }
                                                                        });

                                                                break;
                                                            }



                                                        }

                                                    }else{
                                                        Log.d("DashboardActivity", "Error:", task.getException());
                                                    }
                                                }
                                            });



                                    }
                                break;
                                }
                            }else{
                                Log.w("tag", "onComplete: "+ task.getException().getMessage());
                            }
                        }
                    });

            // check if the user has been initialized


        }
    }

    @Override
    protected void onStart() {
        super.onStart();


        if(deviceNumber == null){

        }else{
            // get the latest lat from the database
            // get the latest lat from the database+

            checkCredit(documentID);

            db.collection("users").document(documentID).collection("tracking")
                    .orderBy("timestamp", Query.Direction.DESCENDING).limit(20)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()){

                                indexCount = 0;

                                for (QueryDocumentSnapshot documentSnapshot : task.getResult()){



                                    final String documentIdentifier = documentSnapshot.getId();

                                    Date time = (Date) documentSnapshot.get("timestamp");
                                    Long timestamp = time.getTime();

                                    Long nowTime = new Date().getTime()-12*60*60*1000;

                                    // timestamp 1200 nowTime 1102

                                    if (timestamp > nowTime){
                                        Log.i("timeSkip", timestamp+" no "+nowTime+" "+ documentIdentifier);

                                    }else{

                                        db.collection("users").document(documentID).collection("tracking")
                                                .document(documentIdentifier).delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.d("taf", "DocumentSnapshot successfully deleted!");
                                                Log.i("timeSkip", documentID);
                                            }
                                        })
                                                .addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception e) {
                                                        Log.w("tag", "Error deleting document", e);
                                                    }
                                                });
                                        ;


                                    }

                                    if (indexCount < 1){
                                        try{

                                            // check current data
                                            lat = documentSnapshot.getData().get("lat").toString();
                                            longi = documentSnapshot.getData().get("long").toString();


                                            if(!lat.equals("null")){
                                                String latitudeRaw = lat;
                                                String longitudeRaw = longi;

                                                Double latitude = Double.parseDouble(latitudeRaw);
                                                Double longitude = Double.parseDouble(longitudeRaw);


                                                Geocoder geocoder;
                                                List<Address> addresses = null;
                                                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                                                try {
                                                    addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                                } catch (IOException e1) {
                                                    e1.printStackTrace();

                                                }

                                                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                                String city = addresses.get(0).getLocality();
                                                String state = addresses.get(0).getAdminArea();
                                                String country = addresses.get(0).getCountryName();
                                                String postalCode = addresses.get(0).getPostalCode();
                                                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                                                if (state == null){
                                                    state = " ";
                                                }

                                                TextView nowAt = findViewById(R.id.nowatAddress);
                                                nowAt.setText(knownName+" "+city+" "+state);
                                                Log.i("result", " "+knownName+" "+documentSnapshot.getData().get("lat"));


                                            }



                                        }catch(NullPointerException e){
                                            Log.i("locationError", e.getMessage());
                                        }
                                    }

                                    indexCount++;
                                }

                            }else{

                            }
                        }
                    });



            db.collection("users").document(documentID).collection("status")
                    .whereEqualTo("device", deviceNumber)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()){


                                available = "no";

                                for (QueryDocumentSnapshot document : task.getResult()){
                                    try{
                                        String power = (String) document.getData().get("power");
                                        String arm = (String) document.getData().get("arm");
                                        String monitor = (String) document.getData().get("monitor");
                                        ImageView powerButton = findViewById(R.id.powerButton);
                                        ImageView armImage = findViewById(R.id.arm);


                                        if (power.equals("on")){
                                            powerButton.setImageResource(R.drawable.power);
                                            available = "yes";
                                        }

                                        if (power.equals("off")){
                                            powerButton.setImageResource(R.drawable.power2);
                                            available = "yes";
                                        }

                                        if (arm.equals("on")){
                                            armImage.setImageResource(R.drawable.arm2);
                                            //                                    Log.d("DashboardActivity", "arm");
                                        }else{
                                            armImage.setImageResource(R.drawable.arm);
                                        }

                                        if (monitor.equals("on")){
                                            sendSMS(deviceNumber, "tracker"+password);

                                            HashMap<String, Object> map = new HashMap<>();
                                            map.put("monitor", "off");

                                            db.collection("users").document(documentID).collection("status").document(documentID)
                                                    .update(map);
                                        }

                                    }catch(NullPointerException e){

                                        Log.e("checkError", e.getMessage());

                                            HashMap<String, Object> defaults = new HashMap<>();
                                            defaults.put("monitor", "off");
                                            defaults.put("power", "on");
                                            defaults.put("arm", "off");
                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                .update(defaults);
                                    }


                                    try {
                                        String refreshRate = document.getData().get("refresh").toString();
                                        Log.i("databaseRefresh", refreshRate);

                                        long startTime;
                                        Handler mHandler = new Handler();

                                        timer = new Timer();

                                        switch (refreshRate){
                                            case "1hr":

                                                startTime = 3600 * 1000;

                                                timer.schedule(new TimerTask() {
                                                    @Override
                                                    public void run() {
                                                        sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                                    }
                                                },startTime, startTime);
                                                //Log.i("time", Long.toString(startTime) );

                                                break;

                                            case "2hr":

                                                startTime = 14400 *1000;
                                                timer.schedule(new TimerTask() {
                                                    @Override
                                                    public void run() {
                                                        sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                                    }
                                                },startTime, startTime);

                                                break;

                                            case "INS":

                                                //sendSMS(deviceNumber, "fix030s001n"+passwordString);


                                            case "minute":

                                                startTime = 60 *1000;
                                                mHandler.postDelayed(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        //sendSMS(deviceNumber, "fix030s001n"+passwordString);
                                                    }
                                                }, startTime);


                                            default:
                                                //sendSMS(deviceNumber, "fix030s001n"+passwordString);

                                        }

                                        TextView textView = findViewById(R.id.rate);
                                        textView.setText(refreshRate);

                                    }catch (NullPointerException e){
                                        HashMap<String, Object> map = new HashMap<>();
                                        map.put("refresh", "1hr");

                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                .update(map);
                                    }

                                }

                                if (available == "no"){

                                    HashMap<String, Object> stopUpdate = new HashMap<>();
                                    stopUpdate.put("device", deviceNumber);
                                    stopUpdate.put("power", "off");

                                    db.collection("users").document(documentID).collection("status").document(documentID)
                                            .set(stopUpdate);
                                }

                            }
                        }
                    });

        }


    }

    @Override
    protected void onStop() {
        super.onStop();



    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try{
            // change the method back to the default
            HashMap<String, Object> map = new HashMap<>();
            map.put("refresh", "1hr");

            db.collection("users").document(documentID).collection("status").document(documentID)
                    .update(map);
        }catch (NullPointerException e){

        }
    }
}
