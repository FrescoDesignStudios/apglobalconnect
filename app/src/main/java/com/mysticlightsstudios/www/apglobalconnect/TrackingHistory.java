package com.mysticlightsstudios.www.apglobalconnect;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TrackingHistory extends AppCompatActivity implements OnMapReadyCallback{

    private GoogleMap mMap;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    public String title;
    private String device;

    Polyline line;
    Context context;

    //static Latlong

    LatLng startingPoint, endingPoint;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tracking_history);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        SharedPreferences preferences = getSharedPreferences("device", MODE_PRIVATE);
        device = preferences.getString("device", null);

        // retrieve the location code of the unit current location
        String UID = mAuth.getCurrentUser().getUid().toString();
        db.collection("users").document(UID).collection("tracking")
                .whereEqualTo("device", device)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            //mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
                            mMap.addTileOverlay(new TileOverlayOptions().tileProvider(new CustomMapTileProvider(getResources().getAssets())));

                            int i = 0;
                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()){

                                String lat = documentSnapshot.getData().get("lat").toString();
                                String longi = documentSnapshot.getData().get("long").toString();
                                String ID = documentSnapshot.getData().get("device").toString();

                                double latDouble = Double.parseDouble(lat);
                                double longDouble = Double.parseDouble(longi);
                                float zoomLevel = (float) 12.0;


                                addMarkersPosition(latDouble, longDouble, zoomLevel, ID, i);
                                i++;

                            }

                            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                                @Override
                                public boolean onMarkerClick(Marker marker) {
                                    // perform an action on click

                                    return false;
                                }
                            });

                        }else{

                        }
                    }
                });
    }

    private GeoApiContext getGeoContext(){
        GeoApiContext geoApiContext = new GeoApiContext();

        return geoApiContext.setQueryRateLimit(3)
                .setApiKey("AIzaSyAymATuUhPSlSPdCB952sBmtp4_qs0aHP0")
                .setConnectTimeout(100, TimeUnit.SECONDS)
                .setReadTimeout(100, TimeUnit.SECONDS)
                .setWriteTimeout(100, TimeUnit.SECONDS);
    }

    public void addMarkersPosition(double latDouble, double longDouble, float zoomLevel, String ID, int i){
        //CameraUpdate upd = CameraUpdateFactory.newLatLngZoom(new LatLng(LAT, LON), ZOOM);
        // Add marker and set map location of the last loaded unit.


        if(i == 0){
            startingPoint = new LatLng(latDouble, longDouble);
            mMap.addMarker(new MarkerOptions().position(startingPoint).title(ID));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(startingPoint));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startingPoint, zoomLevel));
        }

        if (i == 1)
        {
            endingPoint = new LatLng(latDouble, longDouble);
            mMap.addMarker(new MarkerOptions().position(endingPoint).title(ID));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(endingPoint));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(endingPoint, zoomLevel));
        }
/*
        // set time date for the API

        DateTime now = new DateTime();

        DirectionsResult result;

        try {
            result = DirectionsApi.newRequest(getGeoContext())
                    .mode(TravelMode.DRIVING).origin(String.valueOf(startingPoint))
                    .destination(String.valueOf(endingPoint)).departureTime(now)
                    .await();
        } catch (ApiException e) {
            e.printStackTrace();
            Log.i("Polyline", "addMarkersPosition: "+e.toString());
            result = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
            Log.i("PolylineInterrupted", "addMarkersPosition: "+e.toString());
            result = null;
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("PolylineIOException", "addMarkersPosition: "+e.toString());
            result = null;
        }

        if (result.equals(null)){

        }else{
            List<LatLng> decodedPath = PolyUtil.decode(result.routes[0].overviewPolyline.getEncodedPath());
            mMap.addPolyline(new PolylineOptions().addAll(decodedPath));
        }

*/

    }
}
