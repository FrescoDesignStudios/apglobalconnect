package com.mysticlightsstudios.www.apglobalconnect;

import android.content.Intent;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mysticlightsstudios.www.pnp4.TransactionEngine;
import com.mysticlightsstudios.www.pnp4.TransactionEngineException;

import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;


public class PaymentActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    String documentID, email, address, town, parish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_payment);

        Intent intent = getIntent();
        String paymentType = intent.getStringExtra("paymentType");

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


        if (mAuth.getCurrentUser() == null){
            // direct the user back to the login screen

            Intent login = new Intent(PaymentActivity.this, MainActivity.class);
            startActivity(login);
        }else{
            getDocumentID();
            getUserData();
        }

        Spinner cardName = findViewById(R.id.cardName);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.cardNames, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cardName.setAdapter(adapter);

        switch (paymentType){
            case "subscription":

                Spinner amountPaidSub = findViewById(R.id.payList);
                ArrayAdapter<CharSequence> adapterAuto = ArrayAdapter.createFromResource(this, R.array.paymentsSub, android.R.layout.simple_spinner_item);
                adapterAuto.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

                amountPaidSub.setAdapter(adapterAuto);

                TextView amount = findViewById(R.id.amountPay);
                amount.setText("Price you pay per month");

                break;

            case "payAsYouGo":


                Spinner amountPaid = findViewById(R.id.payList);
                ArrayAdapter<CharSequence> adapterManual = ArrayAdapter.createFromResource(this, R.array.payments, android.R.layout.simple_spinner_item);
                adapterManual.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

                amountPaid.setAdapter(adapterManual);




                amountPaid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Log.i("onCreate", parent.getSelectedItem().toString()+ " " + position);

                        TextView amount = findViewById(R.id.amountPay);

                        switch (position){

                            case 1:

                                amount.setText("Your selected package give you 25 app credits");

                                break;

                            case 2:
                                amount.setText("Your selected package give you 50 app credits");
                                break;

                            case 3:
                                amount.setText("Your selected package give you 125 app credits");
                                break;

                            case 4:
                                amount.setText("Your selected package give you 250 app credits");
                                break;

                                default:
                                    amount.setText("Please select a package below to purchase");
                        }


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        TextView amount = findViewById(R.id.amountPay);
                        amount.setText("Select the package from the list below");

                    }
                });




                break;

                default:


        }
    }

    public void getUserData(){


    }

    public void paynow(View view){
        // collect information from the form
        EditText nameEditText = findViewById(R.id.nameOnCard);
        EditText cardNumberEditText = findViewById(R.id.cardNumber);
        EditText cvvEditText = findViewById(R.id.cvv);
        EditText monthEditText = findViewById(R.id.month);
        EditText yearEditText = findViewById(R.id.year);
        Spinner cardType = findViewById(R.id.cardName);
        Spinner funding = findViewById(R.id.payList);


        final String name =  nameEditText.getText().toString();
        final String cardNumber = cardNumberEditText.getText().toString();
        final String cvv = cvvEditText.getText().toString();
        final String cardName = cardType.getSelectedItem().toString();
        String selectedCredit = funding.getSelectedItem().toString();
        String month = monthEditText.getText().toString();
        String year = yearEditText.getText().toString();

        String price; // this is the result from the dropdown spinner

        final String monthYear = month+"/"+year;


            switch (selectedCredit){
                case "$150 JMD":
                    price = "150";
                    break;

                case "$250 JMD":
                    price = "250";
                    break;

                case "$550 JMD":
                    price = "550";
                    break;

                case "$1050 JMD":
                    price = "1050";
                    break;

                    default:
                        price = "1500";

        }



        final String finalPrice = price;

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {

                try  {
                    processPayment(name, cardNumber, cvv, monthYear, finalPrice, email, cardName);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();
    }

    public void getDocumentID(){
        String uid = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID", uid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if (task.isSuccessful()){

                            for (QueryDocumentSnapshot snapshot : task.getResult()){

                                documentID = snapshot.getId();

                                db.collection("users").document(documentID)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                if (task.isSuccessful()){

                                                    DocumentSnapshot snapshot = task.getResult();

                                                    email = snapshot.get("email").toString();
                                                    address = snapshot.get("address").toString();
                                                    town = snapshot.get("town").toString();
                                                    parish = snapshot.get("parish").toString();
                                                }else{}
                                            }
                                        });

                            }
                        }else{}
                    }
                });



    }

    public void checkBillingExist(final String credit, final String subType){

        db.collection("users").document(documentID).collection("billing").document(documentID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){

                            DocumentSnapshot snapshot = task.getResult();
                            Date dateTime = new Date();
                            Timestamp now = new Timestamp(dateTime);

                            HashMap<String, Object> billingUpdate = new HashMap<>();
                            billingUpdate.put("credit", credit);
                            billingUpdate.put("type", subType);
                            billingUpdate.put("timestamp", now);

                            if(snapshot.exists()){

                                db.collection("users").document(documentID).collection("billing").document(documentID).update(billingUpdate);
                                Log.d("checkBillingExist", "onComplete: Yes");
                            }else{
                                db.collection("users").document(documentID).collection("billing").document(documentID).set(billingUpdate);
                                Log.d("checkBillingExist", "onComplete: no");
                            }


                        }else{}
                    }
                });
    }

    public void runSuccessTransaction(HashMap transaction){

        // store results

        db.collection("users").document(documentID).collection("transactions").add(transaction);

        String status = transaction.get("FinalStatus").toString();

        String amountcharged = transaction.get("amountcharged").toString();
        String credit;
        String subType;

        switch (amountcharged){
            case "150.00":
                subType = "PAYG";
                credit = "25";
                //amount.setText("Your selected package give you 25 app credits");

                break;

            case "250.00":
                subType = "PAYG";
                credit = "50";
                //amount.setText("Your selected package give you 50 app credits");
                break;

            case "550.00":
                subType = "PAYG";
                credit = "125";
                //amount.setText("Your selected package give you 125 app credits");
                break;

            case "1050.00":
                subType = "PAYG";
                credit = "250";
                //amount.setText("Your selected package give you 250 app credits");
                break;

                default:
                    credit = "1500";
                    subType = "subscription";

        }


        checkBillingExist(credit, subType);

        // update user credit if Pay As You Go

        //
        if (status.equals("success")){
            Intent thankYou = new Intent(PaymentActivity.this, ThankYouActivity.class);
            thankYou.putExtra("orderNumber", transaction.get("orderID").toString());
            thankYou.putExtra("amountcharged", transaction.get("amountcharged").toString());
            thankYou.putExtra("card-country", transaction.get("card-country").toString());
            startActivity(thankYou);
        }


    }

    public void processPayment(String name, String cardNumber, String cvv, String monthYear, String amount, String email, String cardName){
        TransactionEngine engine = new TransactionEngine();

        Properties pairs = new Properties();

        pairs.put("publisher-name","demo");
        //pairs.put("mode", "payment");
        pairs.put("currency", "JMD");

        pairs.put("card-name",name);
        pairs.put("card-number",cardNumber); //4111 1111 1111 1111
        pairs.put("card-cvv",cvv);
        pairs.put("card-exp",monthYear);

        pairs.put("card-address1", address);
        pairs.put("card-city", town);
        pairs.put("card-state", parish);
        pairs.put("card-zip","0000");
        pairs.put("card-country","JM");
        pairs.put("card-amount", amount); //amount
        pairs.put("email",email);
        pairs.put("card-type",cardName);
        pairs.put("publisher-email","me@mystoreaddress.com");

        // run the transaction and handle any exceptions

        try {
            Log.i("processPayment", "API VERSION: "+ engine.apiVersion());

            Properties results = engine.doTransaction(pairs);

            // save data in database
            //HashMap<String, Object> map = new HashMap<>();
            HashMap<String, Object> newTransaction = new HashMap<>();

            for (Enumeration e = results.propertyNames(); e.hasMoreElements();){
                String key = e.nextElement().toString();

                Log.i("processPayment", key +": "+ results.getProperty(key));


                newTransaction.put(key, results.getProperty(key));


                if (key.equals("MErrMsg")){

                    final String finalKey = key;
                    final String property = results.getProperty(key);
                    final String datea = monthYear;

                    PaymentActivity.this.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(), property, Toast.LENGTH_LONG).show();


                            String[] split = property.split("\\|");

                            for (int i = 0; i < split.length; i++){
                                Toast.makeText(getApplicationContext(), split[i], Toast.LENGTH_LONG).show();
                            }

                        }
                    });


                }

                if (key.equals("FinalStatus")){

                    String status = results.getProperty(key);
                    if (status.equals("success")){


                        //runSuccessTransaction(newTransaction);

                    }else{

                    }

                }
            }

            runSuccessTransaction(newTransaction);

        }catch (TransactionEngineException e){
            // You should handle the exception here some how
            // the messages are fairly meaningful and can be found
            // in TransactionEngine.java
            System.out.println(e.getMessage());
        }

    }
}
