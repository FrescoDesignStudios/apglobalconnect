package com.mysticlightsstudios.www.apglobalconnect;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.webkit.WebHistoryItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;

public class AlertActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    public String deviceNumber;
    public String devicePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_alerts);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        checkPermissions();


        // get all information for the units

        String currentUser = mAuth.getUid();

        db.collection("users").whereEqualTo("UID", currentUser).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            for (final QueryDocumentSnapshot snapshot : task.getResult()){

                                final String document = snapshot.getId();
                                // car name
                                db.collection("users").document(document).collection("devices")
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()){


                                                    for (QueryDocumentSnapshot snapshotDoc : task.getResult()){

                                                        final String carName = snapshotDoc.get("brand").toString();
                                                        final String carModel = snapshotDoc.get("model").toString();
                                                        final String carYear = snapshotDoc.get("year").toString();
                                                        final String number = snapshotDoc.get("device").toString();
                                                        final String password = snapshotDoc.get("password").toString();

                                                        sendCheckUpdate(number, password);



                                                        db.collection("users").document(document).collection("status").document(document)
                                                                .get()
                                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                                        if (task.isSuccessful()){

                                                                            final LinearLayout linearLayout = findViewById(R.id.upperContainer);
                                                                            TextView textView1 = new TextView(getApplicationContext());


                                                                            textView1.setText(carName.toUpperCase()+" "+carModel.toUpperCase()+" "+carYear);
                                                                            TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 2.0f);
                                                                            params.gravity = Gravity.CENTER;
                                                                            params.topMargin = 40;
                                                                            textView1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                                                                            textView1.setLayoutParams(params);
                                                                            textView1.setTextColor(Color.BLUE);


                                                                            DocumentSnapshot documentSnapshot = task.getResult();


                                                                            final TableLayout layout = findViewById(R.id.container);

                                                                            textView1.setOnClickListener(new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View v) {

                                                                                    //TableLayout layout = findViewById(R.id.container);
                                                                                    overLayMenu(document, number, password);

                                                                                    deviceNumber = number;
                                                                                    devicePassword = password;


                                                                                }
                                                                            });

                                                                            layout.addView(textView1);


                                                                            ////////////////////// DOOR ////////////////////////////////

                                                                            TableRow tableRow2 = new TableRow(getApplicationContext());
                                                                            tableRow2.setBackgroundColor(Color.WHITE);

                                                                            tableRow2.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));


                                                                            layout.addView(tableRow2);

                                                                            LinearLayout layout2 = new LinearLayout(getApplicationContext());
                                                                            layout2.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,TableRow.LayoutParams.MATCH_PARENT, 1.0f));
                                                                            layout2.getLayoutParams().width = 0;
                                                                            layout2.setOrientation(LinearLayout.VERTICAL);
                                                                            tableRow2.addView(layout2);

                                                                            TextView textView2 = new TextView(getApplicationContext());
                                                                            textView2.setText("Door");
                                                                            LinearLayout.LayoutParams textParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                                                            textParams.gravity = Gravity.CENTER;
                                                                            textView2.setLayoutParams(textParams);
                                                                            textView2.setTextColor(Color.BLUE);
                                                                            textView2.setPadding(5,5,5,5);
                                                                            textView2.setGravity(View.TEXT_ALIGNMENT_CENTER);

                                                                            layout2.addView(textView2);

                                                                            ImageView imageView = new ImageView(getApplicationContext());
                                                                            LinearLayout.LayoutParams imageParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                                                            imageParam.gravity = Gravity.CENTER;
                                                                            imageView.setLayoutParams(imageParam);

                                                                            try{
                                                                                String door = documentSnapshot.getData().get("Door").toString().replace(" ", "");;

                                                                                switch (door){
                                                                                    case "OFF":
                                                                                        imageView.setImageResource(R.drawable.off);
                                                                                        break;

                                                                                    case "ON":
                                                                                        imageView.setImageResource(R.drawable.on);
                                                                                        break;

                                                                                    default:
                                                                                        imageView.setImageResource(R.drawable.off);
                                                                                }
                                                                            }catch(NullPointerException e){
                                                                                Log.i("door",e.getMessage());
                                                                            }
                                                                            imageView.getLayoutParams().width = 40;
                                                                            imageView.getLayoutParams().height = 40;
                                                                            layout2.addView(imageView);


                                                                            ///////////////////////////// ACC ON //////////////////////

                                                                            LinearLayout accLayout = new LinearLayout(getApplicationContext());
                                                                            accLayout.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                                                                            accLayout.setOrientation(LinearLayout.VERTICAL);
                                                                            accLayout.getLayoutParams().width = 0;
                                                                            tableRow2.addView(accLayout);

                                                                            TextView accText = new TextView(getApplicationContext());
                                                                            accText.setText("ACC");
                                                                            accText.setLayoutParams(textParams);
                                                                            accText.setPadding(5,5,5,5);
                                                                            accText.setTextColor(Color.BLUE);

                                                                            accLayout.addView(accText);

                                                                            ImageView accImage = new ImageView(getApplicationContext());
                                                                            accImage.setLayoutParams(imageParam);
                                                                            try{
                                                                                String acc = documentSnapshot.getData().get("ACC").toString().replace(" ", "");;

                                                                                switch (acc){
                                                                                    case "OFF":
                                                                                        accImage.setImageResource(R.drawable.off);
                                                                                        break;

                                                                                    case "ON":
                                                                                        accImage.setImageResource(R.drawable.on);
                                                                                        break;

                                                                                    default:
                                                                                        accImage.setImageResource(R.drawable.off);
                                                                                }
                                                                            }catch (NullPointerException e){
                                                                                Log.i("acc",e.getMessage());
                                                                            }
                                                                            accImage.getLayoutParams().width = 40;
                                                                            accImage.getLayoutParams().height = 40;
                                                                            accLayout.addView(accImage);


                                                                            /////////////////////// Arm /////////////////////////////

                                                                            LinearLayout armLayout = new LinearLayout(getApplicationContext());
                                                                            armLayout.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                                                                            armLayout.setOrientation(LinearLayout.VERTICAL);
                                                                            armLayout.getLayoutParams().width = 0;
                                                                            tableRow2.addView(armLayout);

                                                                            TextView armText = new TextView(getApplicationContext());
                                                                            armText.setText("ARM");
                                                                            armText.setLayoutParams(textParams);
                                                                            armText.setPadding(5,5,5,5);
                                                                            armText.setTextColor(Color.BLUE);

                                                                            armLayout.addView(armText);

                                                                            ImageView armImage = new ImageView(getApplicationContext());
                                                                            armImage.setLayoutParams(imageParam);
                                                                            try{
                                                                                String arm = documentSnapshot.getData().get("Arm").toString().replace(" ", "");

                                                                                switch (arm){
                                                                                    case "OFF":
                                                                                        armImage.setImageResource(R.drawable.off);
                                                                                        break;

                                                                                    case "ON":
                                                                                        armImage.setImageResource(R.drawable.on);
                                                                                        break;

                                                                                    default:
                                                                                        armImage.setImageResource(R.drawable.off);

                                                                                }
                                                                            }catch (NullPointerException e){
                                                                                Log.i("acc",e.getMessage());
                                                                            }
                                                                            armImage.getLayoutParams().width = 40;
                                                                            armImage.getLayoutParams().height = 40;
                                                                            armLayout.addView(armImage);


                                                                            /////////////////////// oil /////////////////////////////

                                                                            LinearLayout oilLayout = new LinearLayout(getApplicationContext());
                                                                            oilLayout.setLayoutParams(new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
                                                                            oilLayout.setOrientation(LinearLayout.VERTICAL);
                                                                            oilLayout.getLayoutParams().width = 0;
                                                                            tableRow2.addView(oilLayout);

                                                                            TextView oilText = new TextView(getApplicationContext());
                                                                            oilText.setText("Oil");
                                                                            oilText.setLayoutParams(textParams);
                                                                            oilText.setPadding(5,5,5,5);
                                                                            oilText.setTextColor(Color.BLUE);

                                                                            oilLayout.addView(oilText);

                                                                            TextView oilValue = new TextView(getApplicationContext());
                                                                            try {
                                                                                String oil = documentSnapshot.get("Oil").toString();
                                                                                oilValue.setText(oil);

                                                                            }catch (NullPointerException e) {
                                                                                Log.e("oilError", e.getMessage());
                                                                            }

                                                                            oilValue.setLayoutParams(textParams);
                                                                            oilValue.setPadding(5,5,5,5);
                                                                            oilValue.setTextColor(Color.BLACK);

                                                                            oilLayout.addView(oilValue);

                                                                        }else{}
                                                                    }
                                                                });
                                                    }
                                                }else{
                                                    Log.i("deviceFailure", task.getException().toString());
                                                }
                                            }
                                        });
                            }

                        }else{}
                    }
                });
    }

    public void overLayMenu(final String document, final String number, final String password){

        TableLayout options = findViewById(R.id.options);

        options.setVisibility(View.VISIBLE);
        AlphaAnimation animation1 = new AlphaAnimation(0f, 1.0f);
        animation1.setDuration(1000);
        animation1.setStartOffset(100);
        animation1.setFillAfter(true);
        options.startAnimation(animation1);

        Button button = findViewById(R.id.closeEditButton);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Handler mhandler = new Handler();
                mhandler.post(new Runnable() {
                    @Override
                    public void run() {
                        TableLayout tableLayout = findViewById(R.id.options);
                        tableLayout.setVisibility(View.GONE);

                        AlphaAnimation animation1 = new AlphaAnimation(1f, 0f);
                        animation1.setDuration(1000);
                        animation1.setStartOffset(100);
                        animation1.setFillAfter(true);
                        tableLayout.startAnimation(animation1);
                        Intent dash = new Intent(AlertActivity.this, DashboardActivity.class);
                        startActivity(dash);
                    }
                });
            }
        });

        final SwitchCompat governerSwitchState = findViewById(R.id.governerSwitch);
        governerSwitchState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText option = findViewById(R.id.governerSwitchOption);
                String options = option.getText().toString();
                String command1  = "governor"+password;
                String command2 = "nogovernor"+password;
                String command3 = "max"+password+" "+options;

                updateStatus(governerSwitchState, document, number, command1, command2, "governor");
                sendSMS(number, command3);
            }
        });

        final SwitchCompat overSpeedTextState = findViewById(R.id.overSpeedSwitch);
        overSpeedTextState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String command1 = "speed"+password+" 080";
                String command2 = "nospeed"+password;
                updateStatus(overSpeedTextState, document, number,  command1,command2, "overspeed");
            }
        });

        final SwitchCompat movementAlarmSwitch = findViewById(R.id.movementAlarmSwitch);
        movementAlarmSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String command1 = "move"+password+" 0200";
                String command2 = "nomove"+password;
                updateStatus(movementAlarmSwitch, document, number, command1, command2, "movementAlarm");
            }
        });

        final SwitchCompat powerAlarmSwitch = findViewById(R.id.powerAlarmSwitch);
        powerAlarmSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String command1 = "extpower"+password+" on";
                String command2 = "extpower"+password+" off";
                updateStatus(powerAlarmSwitch, document, number, command1, command2, "powerAlarm");
            }
        });

        final SwitchCompat accidentAlarm = findViewById(R.id.accidentAlarmSwitch);
        accidentAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String command1 = "Accident"+password+" on";
                String command2 = "Accident"+password+" off";
                updateStatus(accidentAlarm, document, number, command1,command2, "accidentAlarm");
            }
        });


        final SwitchCompat sleepAlarmSwitch = findViewById(R.id.sleepAlarmSwitch);
        sleepAlarmSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String command1 = "sleep"+password+" on";
                String command2 = "sleep"+password+" off";
                updateStatus(sleepAlarmSwitch, document, number, command1, command2,"sleep");
            }
        });

        final SwitchCompat quickStop = findViewById(R.id.quickStopSwitch);
        quickStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String command1 = "quickstop"+password;
                String command2 = "noquickstop"+password;
                updateStatus(quickStop, document, number, command1, command2,"quickstop");
            }
        });



        //db.collection("users").document(document).collection("devices")

        //part 2 of setting up

        db.collection("users").document(document).collection("status").document(document)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()){

                            DocumentSnapshot snapshot = task.getResult();
                            String governor;
                            String overspeed;
                            String movementAlarm;
                            String powerAlarm;
                            String accidentAlarm;
                            String speedMode;
                            String quickStop;


                            try {
                                governor = snapshot.get("governor").toString();
                                SwitchCompat governerSwitch = findViewById(R.id.governerSwitch);
                                checkState(governor, governerSwitch, document, "governor");


                            }catch (NullPointerException e){}

                            try{
                                accidentAlarm = snapshot.get("accidentAlarm").toString();
                                SwitchCompat accidentAlarmSwitch = findViewById(R.id.accidentAlarmSwitch);
                                checkState(accidentAlarm, accidentAlarmSwitch, document, "accidentAlarm");
                            }catch (NullPointerException e){}


                            try{
                                powerAlarm = snapshot.get("powerAlarm").toString();
                                SwitchCompat powerAlarmSwitch = findViewById(R.id.powerAlarmSwitch);
                                checkState(powerAlarm, powerAlarmSwitch, document, "powerAlarm");
                            }catch (NullPointerException e){}

                            try {
                                movementAlarm = snapshot.get("movementAlarm").toString();
                                SwitchCompat movementAlarmSwitch = findViewById(R.id.movementAlarmSwitch);
                                checkState(movementAlarm, movementAlarmSwitch, document, "movementAlarm");

                            }catch (NullPointerException e){}

                            try{

                                overspeed = snapshot.get("overspeed").toString();
                                SwitchCompat overSpeedSwitch = findViewById(R.id.overSpeedSwitch);
                                checkState(overspeed, overSpeedSwitch, document, "overspeed");

                            }catch (NullPointerException e){
                                Log.i("checkState", "Error: overspeed");

                                updateStatusPointer(document, "overspeed");
                            }

                            try{
                                speedMode = snapshot.get("sleep").toString();
                                SwitchCompat overSpeedSwitch = findViewById(R.id.sleepAlarmSwitch);
                                checkState(speedMode, overSpeedSwitch, document, "sleep");

                            }catch (NullPointerException e){
                                Log.i("checkState", "Error: sleep");

                                updateStatusPointer(document, "sleep");
                            }


                            try {
                                quickStop = snapshot.get("quickstop").toString();
                                SwitchCompat quickStopSwitch = findViewById(R.id.quickStopSwitch);
                                checkState(quickStop, quickStopSwitch, document, "quickstop");
                            }catch (NullPointerException e) {
                                Log.i("checkState", "Error: quickstop");

                                updateStatusPointer(document, "quickstop");

                            }
                        }else{}
                    }
                });
    }

    public void updateStatus( SwitchCompat switchCompat, String document,  String number, String command1, String command2, String field){

                if (switchCompat.isChecked()){
                    db.collection("users").document(document).collection("status").document(document)
                            .update(field,"on");

                    sendSMS(number, command1);
                }else{
                    db.collection("users").document(document).collection("status").document(document)
                            .update(field,"off");

                    sendSMS(number, command2);
                }

    }

    public void checkState(String snapshotString, SwitchCompat resource, String document, String field){

        Log.i("checkState:", "Snap "+field+" "+snapshotString);
        try {

            SwitchCompat switchState = resource;
            switch (snapshotString){
                case "on":

                    switchState.setChecked(true);
                    //Log.i("checkState:", "on");
                    break;

                case "off":

                    switchState.setChecked(false);

                    break;

                default:
                    db.collection("users").document(document).collection("status").document(document)
                            .update(field,"off");
            }

        }catch (NullPointerException e){
            db.collection("users").document(document).collection( "status").document(document)
                    .update(field,"off");

            Log.i("checkState", e.getMessage());
        }
    }

    public void updateStatusPointer(String document,String field){
        db.collection("users").document(document).collection( "status").document(document)
                .update(field,"off");

        Log.i("checkState", field+" pointer");
    }

    public void sendSMS(String phoneNumber, String message){



        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";


        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:

                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));


        if ( ContextCompat.checkSelfPermission( this, Manifest.permission.SEND_SMS ) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions( this, new String[] {  Manifest.permission.SEND_SMS  },
                    3 );
        }else{

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 4);

                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 5);
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
                }else{
                    SmsManager sms = SmsManager.getDefault();
                    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
                }

            }else{
                SmsManager sms = SmsManager.getDefault();
                sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
            }

        }


    }

    public void checkPermissions(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.SEND_SMS}, 1);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, 2);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},3);
        }

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_MMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS}, 4);
        }

    }

    public void sendCheckUpdate(String deviceNumber, String password){

        sendSMS(deviceNumber, "check"+password);

    }

}
