package com.mysticlightsstudios.www.apglobalconnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;

import static android.content.ContentValues.TAG;

public class OutgoingCallReceiver extends BroadcastReceiver {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    public String phoneFromData;
    public String documentID;

    @Override
    public void onReceive(Context context, final Intent intent) {



        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        // check the phone number to make sure that it is the one that was saved in database

        try {
            String UID = mAuth.getCurrentUser().getUid();

            db.collection("users").whereEqualTo("UID", UID)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()){

                                for (QueryDocumentSnapshot documentSnapshot : task.getResult()){
                                    documentID = documentSnapshot.getId();

                                    db.collection("users").document(documentID).collection("status").document(documentID)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if (task.isSuccessful()){

                                                        DocumentSnapshot document = task.getResult();

                                                        if (document.exists()){

                                                            phoneFromData = document.getData().get("device").toString();

                                                            String callState = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
                                                            String phoneNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);

                                                            if (callState == null){
                                                                Log.d("checkState", "onReceive: This state is not ready");
                                                            }else{
                                                                if (callState.equals(TelephonyManager.EXTRA_STATE_IDLE)){

                                                                    if (phoneFromData == phoneNumber){

                                                                        HashMap<String, Object> montitor = new HashMap<>();
                                                                        montitor.put("monitor", "on");

                                                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                                                .update(montitor);
                                                                    }

                                                                }else if (callState.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                                                                    Log.d("checkState", "onReceive: hangup "+ phoneFromData);
                                                                }
                                                            }

                                                        }else{

                                                            Log.d("snapshop", "the document exists not");

                                                        }


                                                    }else{

                                                    }
                                                }
                                            });


                                }

                            }else{

                            }
                        }
                    });
        }catch (NullPointerException e){
            Log.i("Auth", "User not logged in");
        }










        // if call state is ended, change monitor  to track
    }




}
