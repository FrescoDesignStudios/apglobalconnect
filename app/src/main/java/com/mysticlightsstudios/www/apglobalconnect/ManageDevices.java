package com.mysticlightsstudios.www.apglobalconnect;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationSet;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;

public class ManageDevices extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private TableRow tableRow, tableRow2;
    public String TAG = "ManageDevice";
    public String device;


    public void sendSMS(String phoneNumber, String message){

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";


        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:

                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));



        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_manage_devices);



        // loads a list of all the devices that is in use
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        String UID = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID",UID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            for (QueryDocumentSnapshot snapshot : task.getResult()){

                                String documentID = snapshot.getId();



                                db.collection("users").document(documentID).collection("devices")
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()){
                                                    RelativeLayout layout = findViewById(R.id.deviceTable);

                                                    for (QueryDocumentSnapshot snapshot1 : task.getResult()){

                                                        // in the future, use the brand model and plate number

                                                        String brand = snapshot1.getData().get("brand").toString();
                                                        String model = snapshot1.getData().get("model").toString();
                                                        device = snapshot1.getData().get("device").toString();



                                                        TableRow tableRow = new TableRow(getApplicationContext());


                                                        TextView textView = new TextView(getApplicationContext());
                                                        textView.setText(brand+" "+model+" \n");
                                                        textView.setPadding(20,20,20,20);
                                                        textView.setBackgroundColor(Color.GRAY);
                                                        textView.setWidth(240);
                                                        textView.setGravity(Gravity.CENTER);
                                                        textView.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                //create shared preferences

                                                                SharedPreferences.Editor loadDevice = getSharedPreferences("loadedDevice",MODE_PRIVATE).edit();
                                                                loadDevice.putString("deviceID", device);
                                                                loadDevice.commit();

                                                                Log.i("ManageDevice", device);

                                                                //open a new intent with a dashbaord equivalent
                                                                Intent nextDevice = new Intent(ManageDevices.this, SetupActivity.class);
                                                                startActivity(nextDevice);

                                                                //load data from that device
                                                            }
                                                        });
                                                        layout.addView(textView);

                                                    }

                                                }else{

                                                }
                                            }
                                        });

                            }

                        }else{}

                    }
                });


    }
}
