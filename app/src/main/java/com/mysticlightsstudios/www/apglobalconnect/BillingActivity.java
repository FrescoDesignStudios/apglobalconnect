package com.mysticlightsstudios.www.apglobalconnect;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


public class BillingActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private String documentID;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_billing);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


        if (mAuth.getCurrentUser() == null){
            // direct the user back to the login screen

            Intent login = new Intent(BillingActivity.this, MainActivity.class);
            startActivity(login);
        }else{

            // check if the user already has a billing plan

            String documentWorkingID = getDocumentID(mAuth.getUid());

            Log.i("doubleCheck", documentWorkingID);


            //if yes, direct the user to the billing plan information

            //if no, offer user to choose a billing plan

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    public void closeBilling(View view){
        Intent tracking = new Intent(BillingActivity.this, DashboardActivity.class);
        BillingActivity.this.startActivity(tracking);
    }

    public void payAsYouGo(View view){
        Intent payGo = new Intent(BillingActivity.this, PaymentActivity.class);
        payGo.putExtra("paymentType","payAsYouGo");
        startActivity(payGo);
    }

    public void subscribe(View view){
        Intent pay = new Intent(BillingActivity.this, PaymentActivity.class);
        pay.putExtra("paymentType", "subscription");
        startActivity(pay);
    }

    public String getDocumentID(String UID){
        db.collection("users").whereEqualTo("UID", mAuth.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()){

                            for (QueryDocumentSnapshot snapshot :task.getResult()){
                                documentID = snapshot.getId();
                            }

                        }else{
                            Log.i("taskError", task.getException().getMessage());}
                    }
                });

        return documentID+"1";
    }
}
