package com.mysticlightsstudios.www.apglobalconnect;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;

    public void register(View view){
        Intent myIntent = new Intent(MainActivity.this, RegisterActivity.class);
        myIntent.putExtra("user", "test");
        MainActivity.this.startActivity(myIntent);
    }

    public void login(View view){
        EditText username = findViewById(R.id.username);
        EditText password = findViewById(R.id.passwordLogin);

        mAuth.signInWithEmailAndPassword(username.getText().toString(), password.getText().toString())
        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Intent dashboard = new Intent(MainActivity.this, DashboardActivity.class);
                    MainActivity.this.startActivity(dashboard);
                }else{
                    Toast.makeText(getApplicationContext(), "Error: "+ task.getException().getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("DashboardActivity", "Error:", task.getException());
                }
            }
        });

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        }

    @Override
    public void onStart() {
        super.onStart();
        // check if user is signed in

        if (mAuth.getCurrentUser() == null){
            Toast.makeText(getApplicationContext(), "No user", Toast.LENGTH_LONG).show();
        }else{
            Intent dashboard = new Intent(MainActivity.this, DashboardActivity.class);
            MainActivity.this.startActivity(dashboard);
        }

    }
}
