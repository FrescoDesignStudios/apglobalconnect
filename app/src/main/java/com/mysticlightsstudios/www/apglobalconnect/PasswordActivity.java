package com.mysticlightsstudios.www.apglobalconnect;


import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class PasswordActivity extends AppCompatActivity {

    private static final String TAG = "PasswordActivity";
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_password);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


    }

    public void password(View view){

        final EditText password = findViewById(R.id.newPassword);
        String newpassword = password.getText().toString();

        FirebaseUser user = mAuth.getCurrentUser();

        user.updatePassword(newpassword)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        EditText passwordField = findViewById(R.id.newPassword);
                        passwordField.setVisibility(View.GONE);

                        //update message
                        TextView title = findViewById(R.id.title);
                        title.setText("Press the button to go back to login");

                        Button back = findViewById(R.id.save);
                        back.setText("Login");
                        back.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                mAuth.signOut();
                                Intent login = new Intent(PasswordActivity.this, MainActivity.class);
                                startActivity(login);
                            }
                        });
                    }
                });

    }
}
