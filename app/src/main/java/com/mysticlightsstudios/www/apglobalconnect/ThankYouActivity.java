package com.mysticlightsstudios.www.apglobalconnect;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

public class ThankYouActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_thank_you);

        Intent intent = getIntent();

        String orderNumber = intent.getStringExtra("orderNumber");
        String amountcharged = intent.getStringExtra("amountcharged");
        String cardCountry = intent.getStringExtra("card-country");

        String message = "Your card was charged "+amountcharged+" "+cardCountry+" \\n and your order number is "+orderNumber+". " +
                "we will continue to make your car secure.";

        TextView thankYou = findViewById(R.id.thankYouMessage);
        thankYou.setText(message);
    }

    public void backToMain(View view){
        Intent main = new Intent(ThankYouActivity.this, DashboardActivity.class);
        startActivity(main);
    }
}
