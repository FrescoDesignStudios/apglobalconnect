package com.mysticlightsstudios.www.apglobalconnect;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DatabaseErrorHandler;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.telephony.SmsMessage;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SmsBroadcastReceiver extends BroadcastReceiver {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    public String test;
    public String sms_sender = "";
    public String senderSMS;
    public String sms_message = "";
    public String UID2;
    public String UID;
    int i = 0;


    @Override
    public void onReceive(Context context, Intent intent) {
        // Get Bungle object contained in the SMS intent passed in


        Bundle bundle = intent.getExtras();
        SmsMessage[] smsm = null;

        if (bundle != null) {

            // Get SMS message
            Object[] pdus = (Object[]) bundle.get("pdus");

            smsm = new SmsMessage[pdus.length];

            for (int i = 0; i < smsm.length; i++) {
                smsm[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                sms_sender = smsm[i].getOriginatingAddress();
                sms_message = smsm[i].getMessageBody();
            }

            // remove one credit from the database


            // Convert message into array using : to separate them

            mAuth = FirebaseAuth.getInstance();
            db = FirebaseFirestore.getInstance();


            if(sms_message.contains("sensor alarm!")){
                 alarmSecuirty(sms_message, sms_sender);

            }else if (sms_message.contains("lat")) {


                if (sms_sender.length() == 10) {
                    senderSMS = sms_sender;
                }

                if (sms_sender.length() == 11) {
                    senderSMS = sms_sender.substring(1);
                }

                if (sms_sender.length() == 12) {
                    senderSMS = sms_sender.substring(2);
                }

                UID2 = mAuth.getCurrentUser().getUid();

                db.collection("users").whereEqualTo("UID", UID2)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                        UID = snapshot.getId();

                                        db.collection("users").document(UID).collection("devices")
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                        if (task.isSuccessful()) {




                                                            for (QueryDocumentSnapshot document : task.getResult()) {

                                                                String deviceNumber = document.getId();

                                                                if (deviceNumber.equals(senderSMS)){
                                                                    // split the message down into manageable pieces
                                                                    String[] values = sms_message.split("\\n");
                                                                    HashMap<String, Object> update = new HashMap<>();
                                                                    for (int index = 0; index < values.length; index++) {
                                                                        String dataValues = values[index];
                                                                        String[] data = dataValues.split(":");
                                                                        String data1 = data[0];

                                                                        try {
                                                                            update.put(data1, data[1]);
                                                                        }catch (ArrayIndexOutOfBoundsException e){
                                                                            update.put(data1, "No data");
                                                                        }
                                                                    }

                                                                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                                                                    update.put("timestamp", timestamp);

                                                                    update.put("device", senderSMS);


                                                                    //String phone = (String) document.getData().get("device");
                                                                    db.collection("users").document(UID).collection("tracking")
                                                                            .add(update);
                                                                }else{
                                                                    // do nothing
                                                                }


                                                            }

                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .get()
                                                                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                                        @Override
                                                                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                                            DocumentSnapshot document = task.getResult();
                                                                            Log.i("tag", "onComplete: " + document.getData());

                                                                            try{
                                                                                String minusCredit = document.getData().get("credit").toString();



                                                                                double minusCreditf = Double.parseDouble(minusCredit);

                                                                                double newFload = minusCreditf - 1;
                                                                                HashMap<String, Object> credits = new HashMap<>();
                                                                                credits.put("credit", newFload);

                                                                                db.collection("users").document(UID).collection("billing").document(UID)
                                                                                        .update(credits);
                                                                            }catch(NullPointerException e){
                                                                                    HashMap<String, Object> newMap = new HashMap<>();
                                                                                    newMap.put("credit", 0);
                                                                                db.collection("users").document(UID).collection("billing").document(UID)
                                                                                        .set(newMap);
                                                                            }catch (NumberFormatException w){
                                                                                // do nothing
                                                                            }


                                                                        }
                                                                    });


                                                        } else {
                                                            Log.d("SmsBroadcastReceiver", "Error getting documents: ", task.getException());
                                                        }
                                                    }
                                                });
                                        break;
                                    }
                                } else {

                                }
                            }
                        });


                Log.d("Sms", senderSMS);


            } else if (sms_message.contains("Stop engine Succeed")) {
                // check if number is in database

                if (sms_sender.length() == 10) {
                    senderSMS = sms_sender;
                }

                if (sms_sender.length() == 11) {
                    senderSMS = sms_sender.substring(1);
                }

                if (sms_sender.length() == 12) {
                    senderSMS = sms_sender.substring(2);
                }

                UID2 = mAuth.getCurrentUser().getUid();

                db.collection("users").whereEqualTo("UID", UID2)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {

                                    for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                        UID = snapshot.getId();

                                        HashMap<String, Object> stopUpdate = new HashMap<>();
                                        stopUpdate.put("device", senderSMS);
                                        stopUpdate.put("power", "off");

                                        db.collection("users").document(UID).collection("status").document(UID)
                                                .update(stopUpdate);


                                        db.collection("users").document(UID).collection("status")
                                                .whereEqualTo("device", senderSMS)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                        if (task.isSuccessful()) {

                                                            HashMap<String, Object> stopUpdate = new HashMap<>();
                                                            stopUpdate.put("device", senderSMS);
                                                            stopUpdate.put("power", "off");


                                                            for (QueryDocumentSnapshot document : task.getResult()) {
                                                                //Log.d("SmsBroadcastReceiver", "here ");
                                                                if (document.exists()) {
                                                                    db.collection("users").document(UID).collection("status").document(UID)
                                                                            .update(stopUpdate);

                                                                } else {
                                                                    db.collection("users").document(UID).collection("status").document(UID)
                                                                            .set(stopUpdate);
                                                                }
                                                            }

                                                        } else {        }          }
                                                });

                                        db.collection("users").document(UID).collection("billing").document(UID)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                        DocumentSnapshot document = task.getResult();
                                                        Log.i("tag", "onComplete: " + document.getData());

                                                        try{
                                                            String minusCredit = document.getData().get("credit").toString();
                                                            double minusCreditf = Double.parseDouble(minusCredit);

                                                            double newFload = minusCreditf - 1;
                                                            HashMap<String, Object> credits = new HashMap<>();
                                                            credits.put("credit", newFload);

                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .update(credits);
                                                        }catch(NullPointerException e){
                                                            HashMap<String, Object> newMap = new HashMap<>();
                                                            newMap.put("credit", 0);
                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .set(newMap);
                                                        }catch (NumberFormatException e){
                                                            // do nothing
                                                        }

                                                    } });

                                        db.collection("users").document(UID).collection("dump").document(UID)
                                                .update("received", "true");
                                    }

                                } else {                               }
                            }
                        });




            } else if (sms_message.contains("Resume engine Succeed")) {

                // check if number is in database
                if (sms_sender.length() == 10) {
                    senderSMS = sms_sender;
                }

                if (sms_sender.length() == 11) {
                    senderSMS = sms_sender.substring(1);
                }

                if (sms_sender.length() == 12) {
                    senderSMS = sms_sender.substring(2);
                }

                if (sms_sender.length() == 13) {
                    senderSMS = sms_sender.substring(3);
                }

                UID2 = mAuth.getCurrentUser().getUid();

                db.collection("users").whereEqualTo("UID", UID2)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {

                                    for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                        UID = snapshot.getId();

                                        db.collection("users").document(UID).collection("status").document(UID)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                        if (task.isSuccessful()) {

                                                            HashMap<String, Object> stopUpdate = new HashMap<>();
                                                            stopUpdate.put("device", senderSMS);
                                                            stopUpdate.put("power", "on");


                                                            DocumentSnapshot document = task.getResult();
                                                            Log.d("SmsBroadcastReceiver", "here ");
                                                            if (document.exists()) {
                                                                db.collection("users").document(UID).collection("status").document(UID)
                                                                        .update(stopUpdate);
                                                                test = "document";
                                                            } else {
                                                                test = "noDocument";
                                                            }


                                                            if (test == null) {
                                                                Log.d("SmsBroadcastReceiver", "here ");
                                                                db.collection("users").document(UID).collection("status").document(UID)
                                                                        .set(stopUpdate);
                                                            }

                                                        } else {

                                                        }
                                                    }
                                                });

                                        db.collection("users").document(UID).collection("billing").document(UID)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                        DocumentSnapshot document = task.getResult();
                                                        Log.i("tag", "onComplete: " + document.getData());

                                                        try{
                                                            String minusCredit = document.getData().get("credit").toString();
                                                            double minusCreditf = Double.parseDouble(minusCredit);

                                                            double newFload = minusCreditf - 1;
                                                            HashMap<String, Object> credits = new HashMap<>();
                                                            credits.put("credit", newFload);

                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .update(credits);
                                                        }catch(NullPointerException e){
                                                            HashMap<String, Object> newMap = new HashMap<>();
                                                            newMap.put("credit", 0);
                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .set(newMap);
                                                        }catch (NumberFormatException w){
                                                            // do nothing
                                                        }

                                                    }

                                                });

                                        db.collection("users").document(UID).collection("dump").document(UID)
                                                .update("received", "true");
                                    }
                                }else {}
                            }
                        });


            } else if (sms_message.contains("monitor ok!")) {

                // check if number is in database
                if (sms_sender.length() == 10) {
                    senderSMS = sms_sender;
                }

                if (sms_sender.length() == 11) {
                    senderSMS = sms_sender.substring(1);
                }

                if (sms_sender.length() == 12) {
                    senderSMS = sms_sender.substring(2);
                } else {
                    senderSMS = sms_sender.substring(3);
                }

                UID2 = mAuth.getCurrentUser().getUid();

                db.collection("users").whereEqualTo("UID", UID2)
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {

                                    for (QueryDocumentSnapshot snapshot : task.getResult()) {
                                        UID = snapshot.getId();
                                        HashMap<String, Object> monitor = new HashMap<>();
                                        monitor.put("monitorStatus", sms_message);

                                        db.collection("users").document(UID).collection("status").document(UID)
                                                .update(monitor);

                                        db.collection("users").document(UID).collection("billing").document(UID)
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                        DocumentSnapshot document = task.getResult();
                                                        Log.i("tag", "onComplete: " + document.getData());

                                                        try{
                                                            String minusCredit = document.getData().get("credit").toString();
                                                            double minusCreditf = Double.parseDouble(minusCredit);

                                                            double newFload = minusCreditf - 1;
                                                            HashMap<String, Object> credits = new HashMap<>();
                                                            credits.put("credit", newFload);

                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .update(credits);
                                                        }catch(NullPointerException e){
                                                            HashMap<String, Object> newMap = new HashMap<>();
                                                            newMap.put("credit", 0);
                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .set(newMap);
                                                        }catch (NumberFormatException w){
                                                            //do nothing
                                                        }

                                                    }});
                                    }

                                } else {

                                }
                            }});


            } else if (sms_message.contains("begin ok!") || sms_message.contains("password ok!")) {
                beginOk(sms_message, sms_sender);
            }else if(sms_message.contains("Tracker is activated")) {
                arm(sms_message, sms_sender);
            }else if(sms_message.contains("Tracker is deactivated")){
                disarm(sms_message, sms_sender);
            }else if(sms_message.contains("sensor alarm!")){
               // alarmSecuirty(sms_message, sms_sender);
                Log.i("testSentor", "receive sensor message");
            }else if(sms_message.contains("IP")){
                check(sms_message, sms_sender);
            }else if (sms_message.contains("set up fail! pls turn off ACC") || sms_message.contains("setup fail, pls close the door")){
                // give the user a message that the engine is still one
                // change the lock icon back to white
                alarmFailACC(sms_message,sms_sender);
            }else if (sms_message.contains("speed")){

            }
        }
    }

    public void alarmFailACC(final String sms_message2, final String sms_sender) {
        if (sms_sender.length() == 10) {
            senderSMS = sms_sender;
        }

        if (sms_sender.length() == 11) {
            senderSMS = sms_sender.substring(1);
        }

        if (sms_sender.length() == 12) {
            senderSMS = sms_sender.substring(2);
        }

        UID2 = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID", UID2)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                UID = snapshot.getId();

                                if (sms_message2.contains("set up fail")){


                                    HashMap<String, Object> update = new HashMap<>();
                                    update.put("arm","off");

                                    //String phone = (String) document.getData().get("device");
                                    db.collection("users").document(UID).collection("status").document(UID)
                                            .update(update);


                                }

                                db.collection("users").document(UID).collection("billing").document(UID)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                DocumentSnapshot document = task.getResult();
                                                Log.i("tag", "onComplete: " + document.getData());

                                                try{
                                                    String minusCredit = document.getData().get("credit").toString();
                                                    double minusCreditf = Double.parseDouble(minusCredit);

                                                    double newFload = minusCreditf - 1;
                                                    HashMap<String, Object> credits = new HashMap<>();
                                                    credits.put("credit", newFload);

                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .update(credits);
                                                }catch(NullPointerException e){
                                                    HashMap<String, Object> newMap = new HashMap<>();
                                                    newMap.put("credit", 0);
                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .set(newMap);
                                                }catch (NumberFormatException e){
                                                    // do nothing
                                                }

                                            } });




                                db.collection("users").document(UID).collection("dump").document(UID)
                                        .update("received", "true");
                            }

                        } else {  }
                    }
                });
    }

    public void beginOk(String sms_message2, final String sms_sender) {

        //check if user exist
       // Log.i("check", "fuctio "+ userID);

        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful()){

                            for (QueryDocumentSnapshot snapshots : task.getResult()){

                                String userID = snapshots.get("UID").toString();

                                String loggedIn = mAuth.getUid();


                                if(userID.equals(loggedIn)){
                                    UID = snapshots.getId();



                                    // check if number is in database
                                    if (sms_sender.length() == 10) {
                                        senderSMS = sms_sender;
                                    }

                                    if (sms_sender.length() == 11) {
                                        senderSMS = sms_sender.substring(1);
                                    }

                                    if (sms_sender.length() == 12) {
                                        senderSMS = sms_sender.substring(2);
                                    }

                                    // create a dump

                                    HashMap<String, Object> dump = new HashMap<>();
                                    dump.put("phone", senderSMS);
                                    dump.put("length", sms_sender.length());
                                    dump.put("message", sms_message);

                                    db.collection("users").document(UID).collection("dump").document(UID)
                                            .set(dump);

                                    db.collection("users").document(UID).collection("devices").document(senderSMS)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if (task.isSuccessful()) {

                                                        DocumentSnapshot document = task.getResult();

                                                        HashMap<String, Object> update = new HashMap<>();
                                                        Log.i("check", document.get("device").toString());
                                                        if (sms_message.equals("begin ok!") || sms_message.equals("password ok!") || sms_message.equals("admin ok!")) {

                                                            update.put("response", sms_message);
                                                            Log.w("SmsBroadcastReceiver", sms_message);
                                                        } else {

                                                            update.put("responseData", sms_message);
                                                        }

                                                        db.collection("users").document(UID).collection("devices").document(senderSMS)
                                                                .update(update);

                                                    }} });

                                    db.collection("users").document(UID).collection("billing").document(UID)
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                    if(task.isSuccessful()){
                                                        DocumentSnapshot document = task.getResult();
                                                        Log.i("tag", "onComplete: "+document.getData());

                                                        try {
                                                            String minusCredit = document.getData().get("credit").toString();
                                                            double minusCreditf = Double.parseDouble(minusCredit);

                                                            double newFload = minusCreditf - 1;

                                                            HashMap<String, Object> credits = new HashMap<>();
                                                            credits.put("credit",newFload);

                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .update(credits);
                                                        }catch (NullPointerException e){
                                                            db.collection("users").document(UID).collection("billing").document(UID)
                                                                    .update("credit", "120");
                                                        }catch (NumberFormatException w){
                                                            // do nothing
                                                        }




                                                    }else{}
                                                }});


                                    break;
                                }

                            }

                        }else{
                            String e = task.getException().getMessage();

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("fail", e.getMessage());
            }
        });

        Log.i("check", "user");

    }

    public void arm(String sms_message2, final String sms_sender) {

        // check if number is in database

        if (sms_sender.length() == 10) {
            senderSMS = sms_sender;
        }

        if (sms_sender.length() == 11) {
            senderSMS = sms_sender.substring(1);
        }

        if (sms_sender.length() == 12) {
            senderSMS = sms_sender.substring(2);
        }

        UID2 = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID", UID2)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                UID = snapshot.getId();



                                db.collection("users").document(UID).collection("status")
                                        .whereEqualTo("device", senderSMS)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {

                                                    HashMap<String, Object> stopUpdate = new HashMap<>();
                                                    stopUpdate.put("device", senderSMS);
                                                    stopUpdate.put("arm", "on");


                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        //Log.d("SmsBroadcastReceiver", "here ");
                                                        if (document.exists()) {
                                                            db.collection("users").document(UID).collection("status").document(UID)
                                                                    .update(stopUpdate);

                                                        } else {
                                                            db.collection("users").document(UID).collection("status").document(UID)
                                                                    .set(stopUpdate);
                                                        }
                                                    }

                                                } else {        }          }
                                        });

                                db.collection("users").document(UID).collection("billing").document(UID)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                DocumentSnapshot document = task.getResult();
                                                Log.i("tag", "onComplete: " + document.getData());

                                                try{
                                                    String minusCredit = document.getData().get("credit").toString();
                                                    double minusCreditf = Double.parseDouble(minusCredit);

                                                    double newFload = minusCreditf - 1;
                                                    HashMap<String, Object> credits = new HashMap<>();
                                                    credits.put("credit", newFload);

                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .update(credits);
                                                }catch(NullPointerException e){
                                                    HashMap<String, Object> newMap = new HashMap<>();
                                                    newMap.put("credit", 0);
                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .set(newMap);
                                                }catch (NumberFormatException w){
                                                    // do nothing
                                                }

                                            } });

                                db.collection("users").document(UID).collection("dump").document(UID)
                                        .update("received", "true");
                            }

                        } else {                               }
                    }
                });

    }

    public void disarm(String sms_message2, final String sms_sender){
        // check if number is in database

        if (sms_sender.length() == 10) {
            senderSMS = sms_sender;
        }

        if (sms_sender.length() == 11) {
            senderSMS = sms_sender.substring(1);
        }

        if (sms_sender.length() == 12) {
            senderSMS = sms_sender.substring(2);
        }

        UID2 = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID", UID2)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                UID = snapshot.getId();

                                db.collection("users").document(UID).collection("status")
                                        .whereEqualTo("device", senderSMS)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {

                                                    HashMap<String, Object> stopUpdate = new HashMap<>();
                                                    stopUpdate.put("device", senderSMS);
                                                    stopUpdate.put("arm", "off");
                                                    stopUpdate.put("securityAlarm", "off");


                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        //Log.d("SmsBroadcastReceiver", "here ");
                                                        if (document.exists()) {
                                                            db.collection("users").document(UID).collection("status").document(UID)
                                                                    .update(stopUpdate);

                                                        } else {
                                                            db.collection("users").document(UID).collection("status").document(UID)
                                                                    .set(stopUpdate);
                                                        }
                                                    }

                                                } else {        }          }
                                        });

                                db.collection("users").document(UID).collection("billing").document(UID)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                DocumentSnapshot document = task.getResult();
                                                Log.i("tag", "onComplete: " + document.getData());

                                                try{
                                                    String minusCredit = document.getData().get("credit").toString();
                                                    double minusCreditf = Double.parseDouble(minusCredit);

                                                    double newFload = minusCreditf - 1;
                                                    HashMap<String, Object> credits = new HashMap<>();
                                                    credits.put("credit", newFload);

                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .update(credits);
                                                }catch(NullPointerException e){
                                                    HashMap<String, Object> newMap = new HashMap<>();
                                                    newMap.put("credit", 0);
                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .set(newMap);
                                                }catch (NumberFormatException w){
                                                    // do nothing
                                                }

                                            } });

                                db.collection("users").document(UID).collection("dump").document(UID)
                                        .update("received", "true");
                            }

                        } else {  }
                    }
                });

    }

    public void alarmSecuirty(final String sms_message2, final String sms_sender){

        if (sms_sender.length() == 10) {
            senderSMS = sms_sender;
        }

        if (sms_sender.length() == 11) {
            senderSMS = sms_sender.substring(1);
        }

        if (sms_sender.length() == 12) {
            senderSMS = sms_sender.substring(2);
        }

        UID2 = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID", UID2)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                UID = snapshot.getId();

                                if (sms_message2.contains("lat")){

                                    // split the message down into manageable pieces
                                    String[] values = sms_message.split("\\n");
                                    HashMap<String, Object> update = new HashMap<>();
                                    for (int index = 0; index < values.length; index++) {
                                        String dataValues = values[index];
                                        String[] data = dataValues.split(":");
                                        String data1 = data[0];

                                        try {
                                            update.put(data1, data[1]);
                                        }catch (ArrayIndexOutOfBoundsException e){
                                            update.put(data1, "No data");
                                        }
                                    }

                                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                                    update.put("timestamp", timestamp);
                                    update.put("device", senderSMS);


                                    update.put("device", senderSMS);
                                    //String phone = (String) document.getData().get("device");
                                    db.collection("users").document(UID).collection("tracking")
                                            .add(update);


                                }

                                db.collection("users").document(UID).collection("status")
                                        .whereEqualTo("device", senderSMS)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                if (task.isSuccessful()) {

                                                    HashMap<String, Object> stopUpdate = new HashMap<>();
                                                    stopUpdate.put("device", senderSMS);
                                                    stopUpdate.put("securityAlarm", "on");


                                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                                        //Log.d("SmsBroadcastReceiver", "here ");
                                                        if (document.exists()) {
                                                            db.collection("users").document(UID).collection("status").document(UID)
                                                                    .update(stopUpdate);

                                                        } else {
                                                            db.collection("users").document(UID).collection("status").document(UID)
                                                                    .set(stopUpdate);
                                                        }
                                                    }

                                                } else {        }          }
                                        });

                                db.collection("users").document(UID).collection("billing").document(UID)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                DocumentSnapshot document = task.getResult();
                                                Log.i("tag", "onComplete: " + document.getData());

                                                try{
                                                    String minusCredit = document.getData().get("credit").toString();
                                                    double minusCreditf = Double.parseDouble(minusCredit);

                                                    double newFload = minusCreditf - 1;
                                                    HashMap<String, Object> credits = new HashMap<>();
                                                    credits.put("credit", newFload);

                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .update(credits);
                                                }catch(NullPointerException e){
                                                    HashMap<String, Object> newMap = new HashMap<>();
                                                    newMap.put("credit", 0);
                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .set(newMap);
                                                }catch (NumberFormatException w){
                                                    // do nothing
                                                }

                                            } });

                                db.collection("users").document(UID).collection("dump").document(UID)
                                        .update("received", "true");
                            }

                        } else {  }
                    }
                });
    }

    public void check(final String sms_message2, final String sms_sender){
        if (sms_sender.length() == 10) {
            senderSMS = sms_sender;
        }

        if (sms_sender.length() == 11) {
            senderSMS = sms_sender.substring(1);
        }

        if (sms_sender.length() == 12) {
            senderSMS = sms_sender.substring(2);
        }

        UID2 = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID", UID2)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                UID = snapshot.getId();

                                if (sms_message2.contains("Power")){

                                    // split the message down into manageable pieces
                                    String[] values = sms_message.split("\\n");
                                    HashMap<String, Object> update = new HashMap<>();
                                    for (int index = 0; index < values.length; index++) {
                                        String dataValues = values[index];
                                        String[] data = dataValues.split(":");
                                        String data1 = data[0];

                                        try {
                                            update.put(data1, data[1]);
                                        }catch (ArrayIndexOutOfBoundsException e){
                                            update.put(data1, "No data");
                                        }
                                    }

                                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                                    update.put("timestamp", timestamp);
                                    update.put("device", senderSMS);


                                    update.put("device", senderSMS);
                                    //String phone = (String) document.getData().get("device");
                                    db.collection("users").document(UID).collection("status").document(UID)
                                            .update(update);


                                }

                                db.collection("users").document(UID).collection("billing").document(UID)
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                                                DocumentSnapshot document = task.getResult();
                                                Log.i("tag", "onComplete: " + document.getData());

                                                try{
                                                    String minusCredit = document.getData().get("credit").toString();
                                                    double minusCreditf = Double.parseDouble(minusCredit);

                                                    double newFload = minusCreditf - 1;
                                                    HashMap<String, Object> credits = new HashMap<>();
                                                    credits.put("credit", newFload);

                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .update(credits);
                                                }catch(NullPointerException e){
                                                    HashMap<String, Object> newMap = new HashMap<>();
                                                    newMap.put("credit", 0);
                                                    db.collection("users").document(UID).collection("billing").document(UID)
                                                            .set(newMap);
                                                }catch (NumberFormatException w) {
                                                    // do nothing
                                                }

                                            } });




                                db.collection("users").document(UID).collection("dump").document(UID)
                                        .update("received", "true");
                            }

                        } else {  }
                    }
                });
    }

}

