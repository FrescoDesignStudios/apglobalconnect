package com.mysticlightsstudios.www.apglobalconnect;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TrackingActivity extends AppCompatActivity implements OnMapReadyCallback, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private String ID, brand, model, plateNumber, documentID;
    protected LocationManager locationManager;
    protected LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_tracking);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        googleApiClient.connect();


        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


    }

    // directions API start here

    private GeoApiContext getGeoContext(){
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3)
                .setApiKey(getString(R.string.directionsApiKey))
                .setConnectTimeout(10, TimeUnit.SECONDS)
                .setReadTimeout(10, TimeUnit.SECONDS)
                .setWriteTimeout(10, TimeUnit.SECONDS)
                .setMaxRetries(3)
                .setRetryTimeout(10, TimeUnit.SECONDS);
    }

    private String getEndLocationTitle(DirectionsResult result){
        return "Time : "+ result.routes[0].legs[0].duration.humanReadable
                + "Distance : "+result.routes[0].legs[0].distance.humanReadable;
    }

    private void addPolyline(DirectionsResult result, GoogleMap mMap){
        List<LatLng> decodedPath = PolyUtil.decode(result.routes[0].overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath));
    }
    // directions API ends here

    public String getPlateNumber(final String device) {

        db.collection("workorder")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (QueryDocumentSnapshot snapshot : task.getResult()) {

                                try{
                                    String theDevice = snapshot.get("device").toString();

                                    if (theDevice.contains(device)) {
                                        plateNumber = snapshot.get("license").toString();
                                        Log.i("TAG", "getPlateNumber: " + plateNumber);
                                        break;
                                    }
                                }catch (NullPointerException e){
                                    Log.e("getPlateNumber", "error getting device ");
                                }
                            }

                            Log.i("TAG", "onComplete: ");

                        } else {
                            Log.i("TAG", "onComplete: " + task.getException());
                        }
                    }
                });


        return plateNumber;

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;

        // retrieve the location code of the unit current location
        String UID = mAuth.getCurrentUser().getUid();

        db.collection("users").whereEqualTo("UID", UID)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            for (final QueryDocumentSnapshot snapshot : task.getResult()) {
                                final String documentID = snapshot.getId();

                                // get all devices of the same device

                                db.collection("users").document(documentID).collection("devices")
                                        .get()
                                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                            @Override
                                            public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                for (QueryDocumentSnapshot snapshot3 : task.getResult()) {
                                                    String deviceNumber = snapshot3.getData().get("device").toString();
                                                    brand = snapshot3.get("brand").toString();
                                                    model = snapshot3.get("model").toString();

                                                    db.collection("users").document(documentID).collection("tracking")
                                                            .orderBy("timestamp", Query.Direction.DESCENDING).limit(1)
                                                            .get()
                                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                    for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                                                                        String lat = documentSnapshot.getData().get("lat").toString();
                                                                        String longi = documentSnapshot.getData().get("long").toString();
                                                                        ID = documentSnapshot.getData().get("device").toString();
                                                                        String speed = documentSnapshot.get("speed").toString();

                                                                        String data = getPlateNumber(ID);


                                                                        // store device in sharedpreference

                                                                        double latDouble = Double.parseDouble(lat);
                                                                        double longDouble = Double.parseDouble(longi);
                                                                        float zoomLevel = (float) 16.0;


                                                                        //CameraUpdate upd = CameraUpdateFactory.newLatLngZoom(new LatLng(LAT, LON), ZOOM);
                                                                        // Add marker and set map location of the last loaded unit.
                                                                        LatLng marker = new LatLng(latDouble, longDouble);
                                                                        mMap.addMarker(new MarkerOptions().position(marker).title(brand + " " + model).snippet("speed: " + speed + " \n plate: " + data).icon(BitmapDescriptorFactory.fromResource(R.drawable.caricon)));
                                                                        mMap.moveCamera(CameraUpdateFactory.newLatLng(marker));
                                                                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker, zoomLevel));
                                                                        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

                                                                        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                                                                            @Override
                                                                            public View getInfoWindow(Marker marker) {
                                                                                return null;
                                                                            }

                                                                            @Override
                                                                            public View getInfoContents(Marker marker) {

                                                                                LinearLayout info = new LinearLayout(getApplicationContext());
                                                                                info.setOrientation(LinearLayout.VERTICAL);

                                                                                TextView title = new TextView(getApplicationContext());
                                                                                title.setTextColor(Color.BLACK);
                                                                                title.setGravity(Gravity.CENTER);
                                                                                title.setTypeface(null, Typeface.BOLD);
                                                                                title.setText(marker.getTitle());

                                                                                TextView snippet = new TextView(getApplicationContext());
                                                                                snippet.setTextColor(Color.GRAY);
                                                                                snippet.setText(marker.getSnippet());

                                                                                info.addView(title);
                                                                                info.addView(snippet);

                                                                                return info;

                                                                            }
                                                                        });

                                                                    }
                                                                }
                                                            });
                                                }
                                            }
                                        });


                            }

                        } else {

                        }
                    }
                });


    }


    public void history(View view) {
        Intent tracking = new Intent(TrackingActivity.this, TrackingHistory.class);
        TrackingActivity.this.startActivity(tracking);
    }

    public void backToMain(View view) {
        Intent tracking = new Intent(TrackingActivity.this, DashboardActivity.class);
        TrackingActivity.this.startActivity(tracking);
    }


    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 3);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 4);
            Toast.makeText(this, "no permission", Toast.LENGTH_LONG).show();
        } else {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);

            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            if(!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                Toast.makeText(this, "Please turn on GPS", Toast.LENGTH_LONG).show();
            }

            LocationServices.getFusedLocationProviderClient(this).getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful()){

                        try {
                            Double newLat = task.getResult().getLatitude();
                            Double newLong = task.getResult().getLongitude();

                            final LatLng myLocationMarker = new LatLng(newLat, newLong);
                            MarkerOptions a = new MarkerOptions().position(myLocationMarker).title("my Location");
                            mMap.addMarker(a);

                            String UID = mAuth.getCurrentUser().getUid();

                            db.collection("users").whereEqualTo("UID", UID)
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                            if (task.isSuccessful()){
                                                for (final QueryDocumentSnapshot snapshot : task.getResult()){
                                                    documentID = snapshot.getId();

                                                    db.collection("users").document(documentID).collection("tracking")
                                                            .orderBy("timestamp", Query.Direction.DESCENDING).
                                                            limit(1)
                                                            .get()
                                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                                    if (task.isSuccessful()){

                                                                        for (QueryDocumentSnapshot snapshot2 :task.getResult()){


                                                                            String carLat = snapshot2.get("lat").toString();
                                                                            String carLong = snapshot2.get("long").toString();

                                                                            Log.i("onLocationChanged", "Info:"+ snapshot2.get("timestamp"));

                                                                            DateTime now = new DateTime();
                                                                            String origin = myLocationMarker.latitude+","+myLocationMarker.longitude;
                                                                            String destination = carLat+","+carLong;

                                                                            try {
                                                                                DirectionsResult result = DirectionsApi.newRequest(getGeoContext())
                                                                                        .mode(TravelMode.DRIVING).origin(origin)
                                                                                        .destination(destination).departureTime(now)
                                                                                        .await();

                                                                                String title = getEndLocationTitle(result);


                                                                                // LatLng myLocationMarker = new LatLng(newLat, newLong);
                                                                                // MarkerOptions mopt = new MarkerOptions().position(myLocationMarker).title(title);

                                                                                // mopt.position(myLocationMarker);

                                                                                addPolyline(result, mMap);

                                                                            } catch (ApiException e) {
                                                                                e.printStackTrace();
                                                                            } catch (InterruptedException e) {
                                                                                e.printStackTrace();
                                                                            } catch (IOException e) {
                                                                                e.printStackTrace();
                                                                            }
                                                                        }
                                                                    }else{}
                                                                }
                                                            });

                                                }
                                            }else{}
                                        }
                                    });

                            Log.i("onConnected", "mLastLocation:"+myLocationMarker.toString());
                        }catch (NullPointerException e){
                            Log.i("onConnected", e.getMessage());
                        }

                    }else{
                        Log.i("onConnected", "mLastLocation:"+task.getException().toString());
                    }


                }
            });
        }




    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onLocationChanged(final Location location) {

        //mMap.clear(); // remove all markers from the map
        //get the destination address
        // set the rout

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 3);
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 4);
            Toast.makeText(this, "no permission", Toast.LENGTH_LONG).show();
        } else {
           // Toast.makeText(this, "permission granted", Toast.LENGTH_LONG).show();
        }



    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i("onConnectionFailed", "detect: "+connectionResult.getErrorMessage());
    }



}
