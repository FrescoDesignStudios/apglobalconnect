package com.mysticlightsstudios.www.apglobalconnect;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.base.MoreObjects;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.annotation.Nullable;

public class SetupActivity extends AppCompatActivity {


    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    public EditText trackerPhoneNumber;
    private String activePhoneNumber;
    String response;
    String deviceNumber;
    private int password;
    public String available; // for check the power state of the car
    public int counter;
    private String documentID;
    private String status;
    public String lat;
    public String longi;

    public void sendSMS(String phoneNumber, String message){

        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";


        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0,
                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0,
                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---
        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:

                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), "Generic failure",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), "No service",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(getBaseContext(), "Null PDU",
                                Toast.LENGTH_SHORT).show();

                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:

                        Toast.makeText(getBaseContext(), "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---

        registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));



        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);


    }

    public void tracking(View view){
        Intent tracking = new Intent(SetupActivity.this, TrackingActivity.class);
        SetupActivity.this.startActivity(tracking);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_setup);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        if (mAuth.getCurrentUser() == null){
            Intent check = new Intent(SetupActivity.this, MainActivity.class);
            SetupActivity.this.startActivity(check);
        }else{

            // check users account info
            final String UID = mAuth.getCurrentUser().getUid();
            db.collection("users")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {

                            if (task.isSuccessful()){

                                for (QueryDocumentSnapshot snapshot :task.getResult()){
                                    //get first device details

                                    String stUID;
                                    try{
                                        stUID = snapshot.getData().get("UID").toString();
                                    }catch (NullPointerException e){
                                        stUID = "null";
                                    }

                                    if (stUID.equals(UID)){
                                        documentID = snapshot.getId();



                                        db.collection("users").document(documentID).collection("devices")
                                                .get()
                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                        if(task.isSuccessful()){

                                                            SharedPreferences preferences = getSharedPreferences("loadedDevice", MODE_PRIVATE);

                                                            String deviceID = preferences.getString("deviceID", "empty");


                                                            for(QueryDocumentSnapshot document : task.getResult()){

                                                                Log.i("showingDevice", deviceID+" "+documentID);


                                                                String number = document.getData().get("device").toString();

                                                                if (number.equals(number)){
                                                                    counter++;
                                                                    TextView carName = findViewById(R.id.carName);
                                                                    if(document.getData().get("model") == null || document.getData().get("brand") == null){
                                                                        carName.setText("No Device");
                                                                        deviceNumber = "No Device";
                                                                    }else{


                                                                        deviceNumber = document.getData().get("device").toString();

                                                                        try {
                                                                            password = Integer.parseInt(document.getData().get("password").toString());
                                                                        }catch (NullPointerException e){
                                                                            password = 0;
                                                                        }


                                                                        if(password == 0){
                                                                            //create a password for device
                                                                            int min = 100000;
                                                                            int max = 999999;
                                                                            int random = new Random().nextInt(max - min) + min;

                                                                            password = random;

                                                                            HashMap<String, Object> map = new HashMap<>();
                                                                            map.put("password", password);

                                                                            db.collection("users").document(documentID).collection("devices").document(number)
                                                                                    .update(map);

                                                                            sendSMS(deviceNumber, "begin123456");
                                                                            sendSMS(deviceNumber, "password"+String.valueOf(password));

                                                                        }
                                                                        String yearString =  document.getData().get("year").toString();

                                                                        carName.setText((String)document.getData().get("brand")+" "+(String)document.getData().get("model")+" "+yearString);

                                                                        // check the response that was stored in the database

                                                                        try {
                                                                            response = (String) document.getData().get("response").toString();
                                                                        }catch (NullPointerException e)
                                                                        {
                                                                            response = "null";
                                                                        }

                                                                        deviceNumber = (String) document.getData().get("device").toString();


                                                                        if(response.equals("begin ok!") || response.equals("password ok!") || response.equals("admin ok!")){

                                                                            String n = document.getData().get("password").toString();


                                                                            if(response.equals("password ok!")){

                                                                            }else{

                                                                            }

                                                                            // set the check tracker to a certian amount of time

                                                                            sendSMS(deviceNumber, "fix030s001n"+n);

                                                                        }else{

                                                                            sendSMS(deviceNumber, "begin123456");
                                                                            sendSMS(deviceNumber, "password123456 "+String.valueOf(password));
                                                                            break;

                                                                        }

                                                                        // get the latest lat from the database
                                                                        db.collection("users").document(documentID).collection("tracking")
                                                                                .orderBy("timestamp", Query.Direction.DESCENDING).limit(1)
                                                                                .get()
                                                                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                                                                                        Log.i("result", " "+task.getResult().toString());

                                                                                        if (task.isSuccessful()){


                                                                                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()){


                                                                                                // check current data
                                                                                                lat = documentSnapshot.getData().get("lat").toString();
                                                                                                longi = documentSnapshot.getData().get("long").toString();

                                                                                                Date finalTime;


                                                                                                Date timestamp = (Date) documentSnapshot.getData().get("timestamp");


                                                                                                if(!lat.equals("null")){
                                                                                                    String latitudeRaw = lat;
                                                                                                    String longitudeRaw = longi;

                                                                                                    Double latitude = Double.parseDouble(latitudeRaw);
                                                                                                    Double longitude = Double.parseDouble(longitudeRaw);


                                                                                                    Geocoder geocoder;
                                                                                                    List<Address> addresses = null;
                                                                                                    geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                                                                                                    try {
                                                                                                        addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                                                                                    } catch (IOException e1) {
                                                                                                        e1.printStackTrace();

                                                                                                    }

                                                                                                    String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                                                                                    String city = addresses.get(0).getLocality();
                                                                                                    String state = addresses.get(0).getAdminArea();
                                                                                                    String country = addresses.get(0).getCountryName();
                                                                                                    String postalCode = addresses.get(0).getPostalCode();
                                                                                                    String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

                                                                                                    if (state == null){
                                                                                                        state = " ";
                                                                                                    }

                                                                                                    TextView nowAt = findViewById(R.id.nowatAddress);
                                                                                                    nowAt.setText(knownName+" "+city+" "+state);
                                                                                                    Log.i("result", " "+knownName+" "+documentSnapshot.getData().get("lat"));
                                                                                                }



                                                                                            }

                                                                                        }else{
                                                                                            Log.i("Fail", "something wrong");
                                                                                        }
                                                                                    }
                                                                                });



                                                                        db.collection("users").document(documentID).collection("status").document(documentID)
                                                                                .get()
                                                                                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                                                                                    @Override
                                                                                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                                                                        if (task.isSuccessful()){


                                                                                            DocumentSnapshot document = task.getResult();
                                                                                            String power;
                                                                                            try{
                                                                                                power = (String) document.getData().get("power");
                                                                                                String monitor = (String) document.getData().get("monitor");


                                                                                                ImageView powerButton = findViewById(R.id.powerButton);
                                                                                                if (power.equals("on")){
                                                                                                    powerButton.setImageResource(R.drawable.power);
                                                                                                    available = "yes";
                                                                                                }

                                                                                                if (power.equals("off")){
                                                                                                    powerButton.setImageResource(R.drawable.power2);
                                                                                                    available = "yes";
                                                                                                }

                                                                                                if (monitor.equals("on")){
                                                                                                    sendSMS(deviceNumber, "tracker"+password);

                                                                                                    HashMap<String, Object> map = new HashMap<>();
                                                                                                    map.put("monitor", "off");

                                                                                                    db.collection("users").document(documentID).collection("status").document(documentID)
                                                                                                            .update(map);
                                                                                                }

                                                                                            }catch (NullPointerException e) {
                                                                                                power = "null";
                                                                                            }


                                                                                            ImageView armImage = findViewById(R.id.arm);


                                                                                            String arm;
                                                                                            try {
                                                                                                arm = (String) document.getData().get("arm");

                                                                                                if (arm.equals("on")){
                                                                                                    armImage.setImageResource(R.drawable.arm2);
                                                                                                    Log.d("DashboardActivity", "arm");
                                                                                                }else{
                                                                                                    armImage.setImageResource(R.drawable.arm);
                                                                                                }

                                                                                            }catch (NullPointerException e){
                                                                                                arm = "null";
                                                                                                e.getStackTrace();
                                                                                            }



                                                                                        }
                                                                                    }
                                                                                });

                                                                        db.collection("users").document(documentID).collection("dump").document(documentID)
                                                                                .addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                                                                    @Override
                                                                                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {


                                                                                        try {
                                                                                            String document = documentSnapshot.getData().get("received").toString();
                                                                                            Log.i("receiveChanged", document);

                                                                                            if (document.equals("true")){
                                                                                                HashMap<String, Object> map = new HashMap<>();
                                                                                                map.put("received","false");
                                                                                                db.collection("users").document(documentID).collection("dump").document(documentID)
                                                                                                        .update(map);

                                                                                                ConstraintLayout layout = findViewById(R.id.overlay);
                                                                                                layout.setVisibility(View.GONE);

                                                                                            }

                                                                                        }catch (NullPointerException es){
                                                                                            HashMap<String, Object> map = new HashMap<>();
                                                                                            map.put("received","false");
                                                                                            db.collection("users").document(documentID).collection("dump").document(documentID)
                                                                                                    .update(map);
                                                                                        }
                                                                                    }
                                                                                });

                                                                        break;
                                                                    }

                                                                }
                                                                }



                                                        }else{
                                                            Log.d("DashboardActivity", "Error:", task.getException());
                                                        }
                                                    }
                                                });

                                    }
                                    break;
                                }
                            }else{
                                Log.w("tag", "onComplete: "+task.getException().getMessage().toString());
                            }
                        }
                    });

            // check if the user has been initialized



        }
    }


}