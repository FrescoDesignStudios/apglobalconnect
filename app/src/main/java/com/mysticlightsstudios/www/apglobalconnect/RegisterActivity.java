package com.mysticlightsstudios.www.apglobalconnect;


import android.content.Intent;
import android.nfc.Tag;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Random;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class RegisterActivity extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private String ready = "no";
    private String UID;
    private String documentID;
    private EditText email;

    public void registration(View view){
        email = findViewById(R.id.email);
        EditText password = findViewById(R.id.password);
        EditText repassword = findViewById(R.id.repPassword);
        EditText authNumber = findViewById(R.id.phone); // use this to check database

        String passwordString = password.getText().toString();
        String repasswordString = repassword.getText().toString();
        final String newAuthNumber = authNumber.getText().toString();

        if(passwordString.equals(repasswordString)){

            Log.i(TAG, "onComplete: "+ newAuthNumber);

            // check if the user data is there

            mAuth.createUserWithEmailAndPassword(email.getText().toString(), passwordString)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            UID = task.getResult().getUser().getUid();
                            db.collection("users").whereEqualTo("mobile",newAuthNumber)
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()){



                                                for (QueryDocumentSnapshot snapshot : task.getResult()){
                                                    if (snapshot.getData().get("mobile").toString().equals(newAuthNumber)){

                                                        documentID = snapshot.getId();

                                                        //add user UID to the database
                                                        HashMap<String, Object> users = new HashMap<>();
                                                        users.put("UID", UID);
                                                        users.put("email", email.getText().toString());

                                                        db.collection("users").document(documentID)
                                                                .update(users);

                                                        // direct the user to the dash
                                                        Intent dash = new Intent(RegisterActivity.this, DashboardActivity.class);
                                                        startActivity(dash);

                                                        break;
                                                    }else{
                                                        Toast.makeText(getApplicationContext(), "please contact your Admin: error: 00001", Toast.LENGTH_LONG).show();
                                                    }
                                                }

                                            }else {
                                                Log.i(TAG, "onComplete: "+ task.getException().getMessage());
                                            }
                                        }
                                    });
                        }
                    });


            //link user data with user UID



        }else{
            Toast.makeText(getApplicationContext(), password.getText().toString(), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();


    }
}